@analyzed = Array.new

def findfile(d, fn, path)
  path.each do |dir|
    Dir.entries(dir).find do |f|
      if(fn == f) then
        return dir
      end
    end
  end
  return d
end

def analyzefile(d, f, path)
  deps = Array.new
  IO.foreach(f) do |l|
    m = /#include *[<\"](.*)[>\"]/.match(l)
    if m then
      d2 = findfile(d, m[1], path)
      s = d2 + m[1]
      if(File.file?(s)) then
        if @analyzed.index(s) == nil then
          analyzefile(d2, s, path)
        end
        @analyzed << s
        deps << s
      end
    end
  end
  if not deps.empty? then
    puts f.gsub(/\.c(pp)?/, ".o") + ": " + deps.join(" ")
  end
end
def analyzedir(d, path)
  Dir.foreach(d) do |f|
    if /.*\.cpp/.match(f) then
      analyzefile(d, d + f, path)
    end
  end
end
path = []
ARGV.each do |a|
  m = /-I(.*)/.match(a)
  if m then
    path << m[1]
  else
    if Dir.exists? a then
      analyzedir(a, path)
    elsif File.file? a then
      analyzefile("./", a, path)
    end
  end
end
puts "%.h:"
puts "\ttouch $@"
