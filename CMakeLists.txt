cmake_minimum_required(VERSION 2.6)
project(CaveStoryMP-Server)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake_modules")

include_directories("server" "common")

# Enable C++11
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")

find_package(SDL)
find_package(SDL_net)
find_package(Jansson)

add_executable(server server/AssignSprites.cpp server/Map.cpp server/NetHandler.cpp server/Object.cpp server/PlayerMP.cpp server/Server.cpp server/Sprites.cpp server/Tsc.cpp server/Weapon.cpp server/ai/ai.cpp server/ai/computer.cpp server/ai/critter.cpp server/ai/first_cave.cpp server/ai/registry.cpp server/ai/shot.cpp server/ai/zzzz.cpp server/ai/npcregu.cpp server/ai/npcplayer.cpp server/ai/sym.cpp server/ai/village.cpp server/ai/curly.cpp server/main.cpp server/misc.cpp server/slope.cpp common/NetStream.cpp siflib/sectSprites.cpp siflib/sif.cpp siflib/sifloader.cpp)

if(JANSSON_FOUND)
  target_link_libraries(server ${JANSSON_LIBRARIES})
endif(JANSSON_FOUND)

if(SDL_NET_FOUND)
  include_directories(${SDL_NET_INCLUDE_DIRS})
  target_link_libraries(server ${SDL_NET_LIBRARIES})
endif(SDL_NET_FOUND)

if(SDL_FOUND)
  include_directories(${SDL_INCLUDE_DIR})
  target_link_libraries(server ${SDL_LIBRARY})
endif(SDL_FOUND)
