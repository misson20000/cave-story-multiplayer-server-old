#ifndef NETSTREAM_H
#define NETSTREAM_H
#include<stdint.h>
#include<string.h>

namespace std {
  class mutex;
}

class NetStream {
public:
  NetStream(int sock);
  template<class type> type read();
  int8_t  readByte();
  int16_t readShort();
  int32_t readInt();
  void    readBytes(char *buf, int size);

  template<class type> void write(type);
  void writeByte(uint8_t);
  void writeShort(uint16_t);
  void writeInt(uint32_t);
  void writeBytes(char *buf, int size);

  void closeSocket();
  int  isClosed() {
    return closed;
  }
  void flush();
  int populateBuffer(int size);
  int bytesAvailable();
  struct {
    void (*disconnect)(void *dat);
    void *disconnect_data;
  } hooks;
private:
  void _flush();

  std::mutex *root_buffer_mutex; //Mutex for general access and reading
  std::mutex *tmp_buffer_mutex; //PopulateBuffer mutex
  std::mutex *write_buffer_mutex;
  void ensure_write_space(size_t size);
  void fillbuffer();
  int sock;
  char *root_buffer;
  char *tmp_buffer;
  char *send_buffer;

  int read_out; //How much externalness has read OUT of root_buffer
  int read_in;  //How much data has been read IN to root_buffer

  int write_in;        //How much data has been written IN to send_buffer
  int write_out; //How much data has been sent OUT to the internet

  int closed;
};

#endif
