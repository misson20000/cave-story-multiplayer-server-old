#include<mutex>
#include<thread>
#include<chrono>
#include<NetStream.h>
#ifdef __MINGW32__
#include<winsock.h>
#else
#include<arpa/inet.h>
#endif
#include<string.h>
#include<unistd.h>
#include<logging.h>

#define BUFFER_SIZE 512

void closedmsg() {
  log_error("Attempt to use closed socket\n");
}

NetStream::NetStream(int sck) {
  sock = sck;
  root_buffer = (char *) calloc(sizeof(char), BUFFER_SIZE);
  tmp_buffer = (char *) calloc(sizeof(char), BUFFER_SIZE);
  send_buffer = (char *) calloc(sizeof(char), BUFFER_SIZE);
  closed = 0;
  read_out = 0;
  read_in = 0;
  write_out = 0;
  write_in = 0;
  root_buffer_mutex = new std::mutex();
  tmp_buffer_mutex = new std::mutex();
  write_buffer_mutex = new std::mutex();
  hooks.disconnect = 0;
  new std::thread(&NetStream::fillbuffer, this);
}
void NetStream::fillbuffer() {
  // This thread has a simple task: Keep the buffer as full as possible.
  while(!closed) {
    root_buffer_mutex->lock(); //Important to prevent deadlocking
    populateBuffer(1);
    root_buffer_mutex->unlock();
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
}

template<class type> type NetStream::read() {
  if(closed) {
    closedmsg();
    return 0;
  }

  root_buffer_mutex->lock();
  populateBuffer(sizeof(type));
  type v = *((type*)(root_buffer+read_out));
  read_out+= sizeof(type);
  root_buffer_mutex->unlock();
  return v;
}

int8_t NetStream::readByte() {
  return read<int8_t>();
}
int16_t NetStream::readShort() {
  return ntohs(read<int16_t>());
}
int32_t NetStream::readInt() {
  return ntohl(read<int32_t>());
}
void NetStream::readBytes(char *buf, int size) {
  if(closed) {
    closedmsg();
    root_buffer_mutex->unlock();
    return;
  }
  
  root_buffer_mutex->lock();
  populateBuffer(size);
  memcpy(buf, root_buffer+read_out, size);
  read_out+= size;
  root_buffer_mutex->unlock();
}

template<class type> void NetStream::write(type a) {
  if(closed) {
    closedmsg();
    return;
  }
  write_buffer_mutex->lock();
  ensure_write_space(sizeof(type));
  *((type*)(send_buffer+write_in)) = a;
  write_in+= sizeof(type);
  write_buffer_mutex->unlock();
}

void NetStream::writeBytes(char *buf, int size) {
  if(closed) {
    closedmsg();
    return;
  }
  write_buffer_mutex->lock();
  ensure_write_space(size);
  memcpy(send_buffer+write_in, buf, size);
  write_in+= size;
  write_buffer_mutex->unlock();
}
void NetStream::writeByte(uint8_t byte) {
  write(byte);
}
void NetStream::writeShort(uint16_t s) {
  write(htons(s));
}
void NetStream::writeInt(uint32_t i) {
  write(htonl(i));
}

void NetStream::flush() {
  write_buffer_mutex->lock();
  _flush();
  write_buffer_mutex->unlock();
}

void NetStream::_flush() {
  int ret;
  while(write_out < write_in) {
    ret = send(sock, send_buffer+write_out, write_in-write_out, 0);
    if(ret < 1) {
      log_error("Socket error\n");
    } else {
      write_out += ret;
    }
  }
  write_out = write_in = 0;
}

void NetStream::ensure_write_space(size_t size) {
  if(write_in+size >= BUFFER_SIZE) {
    _flush(); // the toilet
  }
}

//Read size bytes into the buffer, wrapping around if necessary
int NetStream::populateBuffer(int size) {
  if(read_out+size >= BUFFER_SIZE) {
    memcpy(root_buffer, root_buffer + read_out,
           BUFFER_SIZE - read_out); //Copy remaining bytes to start of buffer
    read_in-=read_out;
    read_out = 0;
  }
  
  if(read_out + size > read_in) { //Do we really need to read stuff in?
    root_buffer_mutex->unlock();
    tmp_buffer_mutex->lock();
    root_buffer_mutex->lock();
    
    if(read_out + size > read_in) { //Still need to?
      int const o = read_out;
      
      while(read_in < o+size) { //Loop until all necessary bytes are read
        int ret;
        int len = BUFFER_SIZE-read_in;
        root_buffer_mutex->unlock(); //Let other thread do stuff
        ret = recv(sock, tmp_buffer, len, 0);
        root_buffer_mutex->lock();
        
        if(ret == 0) { //Socket has closed
          closed = 1;
          
          if(hooks.disconnect)
          { hooks.disconnect(hooks.disconnect_data); }
          
          tmp_buffer_mutex->unlock();
          return -1;
        } else if(ret == -1) { //Socket has errored
          log_error("recv error\n");
          closeSocket();  closed = 1;
          tmp_buffer_mutex->unlock();
          return -1;
        } else {
          memcpy(root_buffer+read_in, tmp_buffer, ret);
          read_in += ret;
        }
      }
    }
    
    tmp_buffer_mutex->unlock();
  } else {
    //tmp_buffer_mutex->unlock();
    return 1;
  }
  
  return 0;
}

int NetStream::bytesAvailable() {
  root_buffer_mutex->lock();
  int n = read_in - read_out;
  root_buffer_mutex->unlock();
  return n;
}

void NetStream::closeSocket() {
  closed = 1;
  close(sock);
  
  if(hooks.disconnect)
  { hooks.disconnect(hooks.disconnect_data); }
}
