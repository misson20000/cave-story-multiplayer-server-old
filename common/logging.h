#include<config.h>

#define log_error(str) fprintf(stderr, "(ERROR) %s:%d: ", __FILE__, __LINE__); fprintf(stderr, str); fputc('\n', stderr)
#define log_warn(str)   printf(str)

#ifdef DEBUG_INFO
#define log_debug(str) fprintf(stderr, "(DEBUG) %s:%d: ", __FILE__, __LINE__); fprintf(stderr, str); fputc('\n', stderr)
#define log_info(str)   printf("(INFO) %s:%d: ", __FILE__, __LINE__);  printf(str); fputc('\n', stdout)

#ifdef FIND_NYI
#define nyi(str)             printf("%s:%d: NYI ", __FILE__, __LINE__); printf(str); fputc('\n', stdout); _Pragma("warning Implement this!")
#else
#define nyi(str)             printf("%s:%d: NYI ", __FILE__, __LINE__); printf(str); fputc('\n', stdout)
#endif

#else
#define log_debug(str)
#define log_info(str)
#define nyi(str)
#endif
