#ifndef STDAI_H
#define STDAI_H

#include<Object.h>
#include<Map.h>
#include<PlayerMP.h>
#include<misc.h>
#include<sounds.h>

void randblink(Object *o, int blinkframe = 1, int blinktime = 8,
               int prob = 120);
void SmokeClouds(Object *o, int nclouds, int rangex=0, int rangey=0,
                 Object *pushbehind=NULL);
void SmokeXY(int x, int y, int nclouds, int rangex=0, int rangey=0,
             Object *pushbehind=NULL);

#define ONTICK(OBJTYPE, FUNCTION) objprop[OBJTYPE].ai_routines.ontick = FUNCTION;
#define ONDEATH(OBJTYPE, FUNCTION) objprop[OBJTYPE].ai_routines.ondeath = FUNCTION;
#define AFTERMOVE(OBJTYPE, FUNCTION) objprop[OBJTYPE].ai_routines.aftermove = FUNCTION;
#define ONSPAWN(OBJTYPE, FUNCTION) objprop[OBJTYPE].ai_routines.onspawn = FUNCTION;

//Provided for compatibility with existing AI procedures
#define ANIMATE(SPEED, FIRSTFRAME, LASTFRAME) {				\
    o->Animate(SPEED, FIRSTFRAME, LASTFRAME);				\
  }

#define ANIMATE_FWD(SPEED) {					      \
    if (++o->animtimer > SPEED)	{				      \
      o->animtimer = 0;						      \
      o->frame++;						      \
    }								      \
  }

#define player o->ClosestPlayer()

#define FACEPLAYER {							\
		    o->dir = (o->CenterX() > player->CenterX()) ? LEFT:RIGHT; \
		    }

#define FACEAWAYPLAYER {			\
    o->dir = (o->CenterX() > player->CenterX()) ? RIGHT:LEFT;	\
  }

#define LIMITX(K)					\
  {							\
    if (o->xinertia > K) o->xinertia = K;		\
    if (o->xinertia < -K) o->xinertia = -K;		\
  }
#define LIMITY(K)					\
  {							\
    if (o->yinertia > K) o->yinertia = K;		\
    if (o->yinertia < -K) o->yinertia = -K;		\
  }

#define pdistlx(K) ( o->PlayerXDistance() <= (K) )
#define pdistly(K) ( o->PlayerYDistance() <= (K) )
#define pdistly2(ABOVE,BELOW)	(pdistly(o->AboveBelow(ABOVE, BELOW)))
#define pdistl(K)  ( pdistlx((K)) && pdistly((K)) )

#define XMOVE(SPD)  { o->xinertia = (o->dir == RIGHT) ? (SPD) : -(SPD); }
#define XACCEL(SPD) { o->xinertia += (o->dir == RIGHT) ? (SPD) : -(SPD); }

#define YMOVE(SPD)  { o->yinertia = (o->dir == DOWN) ? (SPD) : -(SPD); }
#define YACCEL(SPD) { o->yinertia += (o->dir == DOWN) ? (SPD) : -(SPD); }

#define COPY_PFBOX							\
  { sprites[o->sprite].bbox = sprites[o->sprite].frame[o->frame].dir[o->dir].pf_bbox; }

#define sound(SND) {				\
    ITERATE_PLAYERS_MAP(o->map) {		\
      (*i)->getNetHandler()->sound(SND);	\
    }						\
  }

#define effect(x, y, effectn) {				\
    ITERATE_PLAYERS_MAP(o->map) {			\
      (*i)->getNetHandler()->effect(x, y, effectn);	\
    }							\
  }

void ai_animate1(Object *o);
void ai_animate2(Object *o);
void ai_animate3(Object *o);
void ai_animate4(Object *o);
void ai_animate5(Object *o);

#endif
