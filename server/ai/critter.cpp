#include<ai/stdai.h>
#include<sprites.h>

void ai_critter(Object *o) {
  switch(o->state) {
  case 0: {
    if(o->type == OBJ_POWER_CRITTER) {  // altered physics for Power Critter
      o->critter.jumpheight = 0x2800;
      o->critter.jumpgrav = 0x1C;
      o->critter.falldmg = 12;
    } else {
      o->critter.jumpheight = 0;
      o->critter.jumpgrav = 0x40;
      o->critter.falldmg = 3;
      
      if(o->type == OBJ_CRITTER_HOPPING_BLUE ||   // first cave
          o->type == OBJ_CRITTER_HOPPING_GREEN || // egg 1
          o->type == OBJ_CRITTER_HOPPING_AQUA ||  // egg 2
          o->type == OBJ_CRITTER_HOPPING_RED) {	  // last cave
        o->critter.canfly = false;
        
        // critters in egg1 only 2 dmg
        if(o->type == OBJ_CRITTER_HOPPING_GREEN)
        { o->critter.falldmg = 2; }
        
        // critters in First Cave don't jump as high
        if(o->type != OBJ_CRITTER_HOPPING_BLUE) {
          o->critter.jumpgrav = 0x2C;
        }
      } else {
        // critters are purple in Maze
        o->sprite = SPR_CRITTER_FLYING_CYAN;
        
        o->critter.canfly = true;
      }
    }
    
    o->state = 1;
  } //fall thru
  
  case 1: {
    o->frame = 0;
    
    if(o->timer >= 8) {
      int attack_dist = (o->critter.canfly) ? (96 << CSF) : (64 << CSF);
      
      // close enough to attack?
      if(pdistlx(attack_dist) && pdistly2(96<<CSF, 48<<CSF)) {
        o->state = 2;
        o->frame = 0;
        o->timer = 0;
      } else if(pdistlx(attack_dist + (32<<CSF))
                && pdistly2(128<<CSF, 48<<CSF)) {  //Look at player
        FACEPLAYER;
        o->frame = 1;
        o->timer = 8;	// reset timer to stop watching
      } else {
        // once a little time has passed stop watching him if he turns his back
        if((o->x > player->x && player->dir==LEFT) ||			\
            (o->x < player->x && player->dir==RIGHT)) {
          if(++o->timer >= 150) {
            o->frame = 0;
            o->timer = 8;
          }
        } else { o->timer = 8; }
      }
    } else {
      o->timer++;
    }
    
    if(o->shaketime) {	// attack if shot
      o->state = 2;
      o->frame = 0;
      o->timer = 0;
    }
  }
  break;
  
  case 2: { //start jump
    if(++o->timer > 8) {
      o->state = 3;
      o->frame = 2;
      o->yinertia = -1228;
      sound(SND_ENEMY_JUMP);
      
      FACEPLAYER;
      XMOVE(0x100);
    }
  }
  break;
  
  case 3: { // jumping
    // enter flying phase as we start to come down or
    // if we hit the ceiling.
    if(o->yinertia > 0x100 || o->blocku) {
      // during flight we will sine-wave oscilliate around this position
      o->ymark = (o->y - o->critter.jumpheight);
      
      o->state = 4;
      o->frame = 3;
      o->timer = 0;
    } else {
      if(o->blockd
          && o->yinertia >= 0) {  // jumped onto a platform before we got to fly--land immediately
        goto landed;
      }
      
      break;
    }
  }	// fall-thru
  
  case 4: { // flying
    FACEPLAYER;
    
    // time to come down yet?
    // (come down immediately if we are not one of the flying critters)
    if(!o->critter.canfly ||					       \
        o->blockl || o->blockr || o->blocku ||			       \
        ++o->timer > 100) {
      o->damage = o->critter.falldmg; // increased damage if falls on player
      o->state = 5;
      o->frame = 2;
      o->yinertia /= 2;
    } else {
      // run the propeller
      ANIMATE(0, 3, 5);
      
      if((o->timer & 3)==1) { sound(SND_CRITTER_FLY); }
      
      if(o->blockd) { o->yinertia = -0x200; }
    }
  }
  break;
  
  case 5: { // coming down from flight
    if(o->blockd) {  // landed
landed: ;
      o->damage = 2;			// reset to normal damage
      o->state = 1;
      
      o->frame = 0;
      o->timer = 0;
      o->xinertia = 0;
      
      sound(SND_THUD);
    }
  }
  break;
  }
  
  if(o->state == 4) {  //flying
    // fly towards player
    o->xinertia += (o->x > player->x) ? -0x20 : 0x20;
    // sine-wave oscillate
    o->yinertia += (o->y > o->ymark) ? -0x10 : 0x10;
    
    LIMITX(0x200);
    LIMITY(0x200);
  } else if(o->state == 3 && o->yinertia < 0) { // jumping up
    o->yinertia += o->critter.jumpgrav;
  } else {
    o->yinertia += 0x40;
  }
  
  LIMITY(0x5ff);
}
