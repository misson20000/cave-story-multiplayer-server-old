#include<ai/stdai.h>

// shared between OBJ_KAZUMA_AT_COMPUTER and OBJ_SUE_AT_COMPUTER
void ai_npc_at_computer(Object *o) {
  enum { INIT=0, TYPING, PAUSE_SLOUCH, PAUSE_UPRIGHT };
  
  switch(o->state) {
  case 0:
    o->SnapToGround();
    
    o->state = TYPING;
    o->frame = 0;
    
  case TYPING: {
    ANIMATE(2, 0, 1);
    
    if(!random(0, 80)) {
      o->NoAnimation();
      o->state = PAUSE_SLOUCH;
      o->frame = 1;
      o->timer = 0;
    } else if(!random(0, 120)) {
      o->NoAnimation();
      o->state = PAUSE_UPRIGHT;
      o->frame = 2;
      o->timer = 0;
    }
  }
  break;
  
  case PAUSE_SLOUCH: {
    if(++o->timer > 40) {
      o->state = PAUSE_UPRIGHT;
      o->frame = 2;
      o->timer = 0;
    }
  }
  break;
  
  case PAUSE_UPRIGHT: {
    if(++o->timer > 80) {
      o->state = TYPING;
      o->frame = 0;
      o->timer = 0;
    }
  }
  break;
  }
}
