#include<ai/stdai.h>
#include<effects.h>
#include<flags.h>
#include<config.h>

enum run_shot_result {
  RS_NONE = 0,
  RS_HIT_ENEMY,
  RS_HIT_WALL,
  RS_TTL_EXPIRED
};

#define STATE_SHOT_HIT 1

Object *check_hit_enemy(Object *shot, uint32_t flags_to_exclude) {
  Object *enemy;
#ifdef PVP_ENABLED
  ITERATE_PLAYERS_MAP(shot->map) {
    enemy = *i;
    if(enemy != shot->shot.owner && hitdetect(enemy, shot)) {
      return enemy;
    }
  }
#endif
  FOREACH_OBJECT(shot->map) {
    enemy = *i;
    
    if(enemy->flags & (FLAG_SHOOTABLE | FLAG_INVULNERABLE)) {
      if((enemy->flags & flags_to_exclude) == 0) {
        if(hitdetect(enemy, shot)) {
          return enemy;
        }
      }
    }
  }
  return NULL;
}

void shot_spawn_effect(Object *o, int effectno) {
  int x, y;
  
  if((o->type == OBJ_NEMESIS_SHOT && o->shot.level != 2)
      || (o->type == OBJ_MGUN_LEADER)) {
    switch(o->shot.dir) {
    case LEFT:
      x = o->x;
      y = o->CenterY();
      break;
      
    case RIGHT:
      x = (o->x + o->Width());
      y = o->CenterY();
      break;
      
    case UP:
      x = o->CenterX();
      y = o->y;
      break;
      
    case DOWN:
      x = o->CenterX();
      y = (o->y + o->Height());
      break;
    }
  } else {
    x = o->CenterX();
    y = o->CenterY();
  }
  
  if(effectno == EFFECT_STARSOLID || effectno == EFFECT_SPUR_HIT) {
    switch(o->shot.dir) {
    case RIGHT: x += 0x400; break;
    
    case LEFT:  x -= 0x400; break;
    
    case UP:    y -= 0x400; break;
    
    case DOWN:  y += 0x400; break;
    }
  }
  
  effect(x, y, effectno);
}

Object *damage_enemies(Object *o, uint32_t flags_to_exclude=0) {
  Object *enemy;
  
  if((enemy = check_hit_enemy(o, flags_to_exclude))) {
    if(enemy->flags & FLAG_INVULNERABLE) {
      shot_spawn_effect(o, EFFECT_STARSOLID);
      sound(SND_TINK);
    } else {
      enemy->DealDamage(o->shot.damage, o);
    }
    
    return enemy;
  }
  
  return NULL;
}

bool shot_destroy_blocks(Object *o) {
  int x,y;
  SIFPointList *plist;
  
  switch(o->shot.dir) {
  case LEFT: 	plist = &sprites[o->sprite].block_l; break;
  
  case RIGHT:   plist = &sprites[o->sprite].block_r; break;
  
  case UP: 	plist = &sprites[o->sprite].block_u; break;
  
  case DOWN: 	plist = &sprites[o->sprite].block_d; break;
  
  default:	return 0;
  }
  
  if(o->CheckAttribute(plist, TA_DESTROYABLE, &x, &y)) {
    o->map->tiles[x][y]--;
    sound(SND_BLOCK_DESTROY);
    shot_spawn_effect(o, EFFECT_FISHY);
    ITERATE_PLAYERS_MAP(o->map) {
      (*i)->getNetHandler()->update_block(x, y, o->map->tiles[x][y]);
    }
    return 1;
  }
  
  return 0;
}

bool IsBlockedInShotDir(Object *o) {
  switch(o->shot.dir) {
  case LEFT:  return o->blockl;
  
  case RIGHT: return o->blockr;
  
  case UP:    return o->blocku;
  
  case DOWN:  return o->blockd;
  }
  
  return false;
}

uint8_t run_shot(Object *o, bool destroys_blocks) {
  if(damage_enemies(o)) {
    o->Delete();
    return RS_HIT_ENEMY;
  }
  
  if(IsBlockedInShotDir(o)) {
    shot_spawn_effect(o, EFFECT_STARSOLID);
    
    if(destroys_blocks) {
      if(!shot_destroy_blocks(o)) {
        sound(SND_SHOT_HIT);
      }
    }
    
    o->Delete();
    return RS_HIT_WALL;
  }
  
  if(--o->shot.ttl < 0) {
    shot_spawn_effect(o, EFFECT_STARPOOF);
    o->Delete();
    return RS_TTL_EXPIRED;
  }
  
  return 0;
}

void ai_polar_shot(Object *o) {
  if(o->state == 0) {
    if(damage_enemies(o)) {
      o->state = STATE_SHOT_HIT;
    } else if(IsBlockedInShotDir(o)) {
      shot_spawn_effect(o, EFFECT_STARSOLID);
      o->state = STATE_SHOT_HIT;
      
      if(!shot_destroy_blocks(o)) {
        sound(SND_SHOT_HIT);
      }
    } else if(--o->shot.ttl < 0) {
      shot_spawn_effect(o, EFFECT_STARPOOF);
      o->state = STATE_SHOT_HIT;
    }
  }
  
  if(o->state == STATE_SHOT_HIT) {
    o->Delete();
  }
}
