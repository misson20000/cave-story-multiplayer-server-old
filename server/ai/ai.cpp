#include<ai/stdai.h>
#include<flags.h>

// handles object blinking: at random intervals forces object o's frame to blinkframe
// for blinktime frames.
void randblink(Object *o, int blinkframe, int blinktime, int prob) {
  if(o->blinktimer) {
    o->blinktimer--;
    o->frame = blinkframe;
  } else if(random(0, prob) == 0) {
    o->frame = blinkframe;
    o->blinktimer = 8;
  }
}

// call this in an object's aftermove routine if it's an object
// which is being carried by the player like a puppy// or curly. Nope
// x_left: offset from p's action point when he faces left
// x_right: when he faces right
// off_y: vertical offset from p's action point
void StickToPlayer(Object *o, PlayerMP *p, int x_left, int x_right, int off_y) {
  int x, y, frame;
  
  // needed for puppy in chest
  o->flags &= ~FLAG_SCRIPTONACTIVATE;
  
  // by offsetting from the player's action point, where he holds his gun, we
  // already have set up for us a nice up-and-down 1 pixel as he walks
  frame = p->frame;
  
  // the p's "up" frames have unusually placed action points so we have to cancel those out
  if(frame >= 3 && frame <= 5) { frame -= 3; }
  
  x = (p->x >> CSF) + sprites[p->sprite].frame[frame].dir[p->dir].actionpoint.x;
  y = (p->y >> CSF) + sprites[p->sprite].frame[frame].dir[p->dir].actionpoint.y;
  y += off_y;
  
  if(p->dir == RIGHT) {
    x += x_right;
    o->dir = RIGHT;
  } else {
    x += x_left;
    o->dir = LEFT;
  }
  
  if(o->x >> CSF != x
      || o->y >> CSF != y) {
    o->x = (x << CSF);
    o->y = (y << CSF);
  } else {
    o->x = (x << CSF);
    o->y = (y << CSF);
  }
  
}

static void simpleanim(Object *o, int spd) {
  ANIMATE(spd, 0, sprites[o->sprite].nframes-1)
}

void ai_animate1(Object *o) {
  ANIMATE(1, 0, sprites[o->sprite].nframes)
}
void ai_animate2(Object *o) {
  simpleanim(o, 2);
}
void ai_animate3(Object *o) {
  simpleanim(o, 3);
}
void ai_animate4(Object *o) {
  simpleanim(o, 4);
}
void ai_animate5(Object *o) {
  simpleanim(o, 5);
}
