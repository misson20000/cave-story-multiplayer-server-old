#include<Object.h>
#include<ai/stdai.h>
#include<ai/ai.h>
#include<string.h>

bool load_npc_tbl(void) {
  const int smoke_amounts[] = { 0, 3, 7, 12 };
  const int nEntries = 361;
  int i;
  
  FILE *fp = fopen("data/npc.tbl", "rb");
  
  if(!fp) {
    fprintf(stderr, "Could not load npc.tbl\n");
    return 1;
  }
  
  for(i=0; i<nEntries; i++) { fread(&(objprop[i].defaultflags), 2, 1, fp); }
  
  for(i=0; i<nEntries; i++) { fread(&(objprop[i].initial_hp), 2, 1, fp); }
  
  // next is a spritesheet # of something--but we don't use it, so skip
  //for(i=0;i<nEntries;i++) fgetc(fp);		// spritesheet # or something--but we don't use it
  fseek(fp, (nEntries * 2 * 2) + nEntries, SEEK_SET);
  
  for(i=0; i<nEntries; i++) { objprop[i].death_sound = fgetc(fp); }
  
  for(i=0; i<nEntries; i++) { objprop[i].hurt_sound = fgetc(fp); }
  
  for(i=0; i<nEntries; i++) { objprop[i].death_smoke_amt = smoke_amounts[fgetc(fp)]; }
  
  for(i=0; i<nEntries; i++) { fread(&(objprop[i].xponkill), 4, 1, fp); }
  
  for(i=0; i<nEntries; i++) { fread(&(objprop[i].damage), 4, 1, fp); }
  
  /*for(i=0;i<nEntries;i++)
  {
  	int left = fgetc(fp);
  	int top = fgetc(fp);
  	int right = fgetc(fp);
  	int bottom = fgetc(fp);
  
  	if (i == 59)
  	{
  		stat("%d %d %d %d", left, top, right, bottom);
  		stat("sprite %d", objprop[i].sprite);
  	}
  }*/
  
  fclose(fp);
  return 0;//1;
}


void register_ai(void) {

  if(load_npc_tbl()) { return; }
#include "objprops.cpp"
  objprop[OBJ_NULL].defaultflags = 0;
  memcpy(objprop+OBJ_SKULLHEAD_CARRIED, objprop+OBJ_SKULLHEAD, sizeof(ObjProp));
  
  objprop[OBJ_POLISH].initial_hp = 24;
  objprop[OBJ_POLISH].death_sound = 25;
  
  //ontick
  ONTICK(OBJ_CRITTER_HOPPING_BLUE, ai_critter);
  ONTICK(OBJ_BAT_BLUE, ai_bat_up_down);
  ONTICK(OBJ_HERMIT_GUNSMITH, ai_hermit_gunsmith);
  ONTICK(OBJ_DOOR_ENEMY, ai_door_enemy);
  //ONTICK(OBJ_SNAKE1_SHOT, ai_snake);
  //ONTICK(OBJ_SNAKE23_SHOT, ai_snake_23);
  //ONTICK(OBJ_SNAKE_TRAIL, ai_snake_trail);
  //ONTICK(OBJ_BLADE3_SHOT, ai_blade_l3_shot);
  ONTICK(OBJ_POLAR_SHOT, ai_polar_shot);
  //ONTICK(OBJ_MGUN_L1_SHOT, ai_polar_shot);
  ////ONTICK(OBJ_MGUN_LEADER, ai_polar_shot);
  //ONTICK(OBJ_MGUN_TRAIL, ai_mgun_trail);
  //ONTICK(OBJ_MGUN_SPAWNER, ai_mgun_spawner);
  //ONTICK(OBJ_FIREBALL1, ai_fireball);
  //ONTICK(OBJ_FIREBALL23, ai_fireball_level_23);
  //ONTICK(OBJ_FIREBALL_TRAIL, ai_fireball_trail);
  //ONTICK(OBJ_WHIMSICAL_STAR, ai_whimsical_star);
  //ONTICK(OBJ_BUBBLER12_SHOT, ai_bubbler_l12);
  //ONTICK(OBJ_BUBBLER3_SHOT, ai_bubbler_l3);
  //ONTICK(OBJ_BUBBLER_SHARP, ai_bubbler_sharp);
  //ONTICK(OBJ_NEMESIS_SHOT, ai_nemesis_shot);
  //ONTICK(OBJ_NEMESIS_SHOT_CURLY, ai_nemesis_shot);
  //ONTICK(OBJ_SPUR_SHOT, ai_spur_shot);
  //ONTICK(OBJ_SPUR_TRAIL, ai_spur_trail);
  //ONTICK(OBJ_SUE_FRENZIED, ai_sue_frenzied);
  //ONTICK(OBJ_MISERY_FRENZIED, ai_misery_frenzied);
  //ONTICK(OBJ_MISERY_CRITTER, ai_misery_critter);
  //ONTICK(OBJ_MISERY_BAT, ai_misery_bat);
  //ONTICK(OBJ_MISERY_MISSILE, ai_misery_missile);
  //ONTICK(OBJ_HELICOPTER, ai_helicopter);
  //ONTICK(OBJ_HELICOPTER_BLADE, ai_helicopter_blade);
  //ONTICK(OBJ_IGOR_BALCONY, ai_igor_balcony);
  //ONTICK(OBJ_FALLING_BLOCK, ai_falling_block);
  //ONTICK(OBJ_FALLING_BLOCK_SPAWNER, ai_falling_block_spawner);
  //ONTICK(OBJ_BOSS_DOCTOR_FRENZIED, ai_boss_doctor_frenzied);
  //ONTICK(OBJ_DOCTOR_BAT, ai_doctor_bat);
  //ONTICK(OBJ_BOSS_DOCTOR, ai_boss_doctor);
  //ONTICK(OBJ_DOCTOR_SHOT, ai_doctor_shot);
  //ONTICK(OBJ_DOCTOR_SHOT_TRAIL, ai_doctor_shot_trail);
  //ONTICK(OBJ_DOCTOR_BLAST, ai_doctor_blast);
  //ONTICK(OBJ_DOCTOR_CROWNED, ai_doctor_crowned);
  //ONTICK(OBJ_MIMIGA_CAGED, ai_mimiga_caged);
  //ONTICK(OBJ_CHIE_CAGED, ai_mimiga_caged);
  //ONTICK(OBJ_CHACO_CAGED, ai_mimiga_caged);
  //ONTICK(OBJ_SANTA_CAGED, ai_mimiga_caged);
  //ONTICK(OBJ_DOCTOR_GHOST, ai_doctor_ghost);
  //ONTICK(OBJ_RED_ENERGY, ai_red_energy);
  //ONTICK(OBJ_BOSS_MISERY, ai_boss_misery);
  //ONTICK(OBJ_MISERY_PHASE, ai_misery_phase);
  //ONTICK(OBJ_MISERY_SHOT, ai_generic_angled_shot);
  //ONTICK(OBJ_MISERY_RING, ai_misery_ring);
  //ONTICK(OBJ_MISERY_BALL, ai_misery_ball);
  //ONTICK(OBJ_BLACK_LIGHTNING, ai_black_lightning);
  //ONTICK(OBJ_ORANGEBELL, ai_orangebell);
  //ONTICK(OBJ_ORANGEBELL_BABY, ai_orangebell_baby);
  //ONTICK(OBJ_STUMPY, ai_stumpy);
  //ONTICK(OBJ_MIDORIN, ai_midorin);
  //ONTICK(OBJ_GUNFISH, ai_gunfish);
  //ONTICK(OBJ_GUNFISH_SHOT, ai_gunfish_shot);
  //ONTICK(OBJ_DROLL, ai_droll);
  //ONTICK(OBJ_DROLL_SHOT, ai_droll_shot);
  //ONTICK(OBJ_DROLL_GUARD, ai_droll_guard);
  //ONTICK(OBJ_MIMIGA_FARMER_STANDING, ai_mimiga_farmer);
  //ONTICK(OBJ_MIMIGA_FARMER_WALKING, ai_mimiga_farmer);
  //ONTICK(OBJ_ROCKET, ai_rocket);
  //ONTICK(OBJ_PROXIMITY_PRESS_HOZ, ai_proximity_press_hoz);
  //ONTICK(OBJ_PUPPY_ITEMS, ai_puppy_wag);
  //ONTICK(OBJ_NUMAHACHI, ai_numahachi);
  //ONTICK(OBJ_MIMIGA_JAILED, ai_mimiga_farmer);
  //ONTICK(OBJ_ITOH, ai_npc_itoh);
  //ONTICK(OBJ_KANPACHI_STANDING, ai_kanpachi_standing);
  //ONTICK(OBJ_MOMORIN, ai_npc_momorin);
  //ONTICK(OBJ_BEETLE_BROWN, ai_beetle_horiz);
  //ONTICK(OBJ_POLISH, ai_polish);
  //ONTICK(OBJ_POLISHBABY, ai_polishbaby);
  //ONTICK(OBJ_SANDCROC, ai_sandcroc);
  //ONTICK(OBJ_MIMIGAC1, ai_curlys_mimigas);
  //ONTICK(OBJ_MIMIGAC2, ai_curlys_mimigas);
  //ONTICK(OBJ_MIMIGAC_ENEMY, ai_curlys_mimigas);
  //ONTICK(OBJ_SUNSTONE, ai_sunstone);
  //ONTICK(OBJ_ARMADILLO, ai_armadillo);
  //ONTICK(OBJ_CROW, ai_crow);
  //ONTICK(OBJ_CROWWITHSKULL, ai_crowwithskull);
  //ONTICK(OBJ_SKULLHEAD, ai_skullhead);
  //ONTICK(OBJ_SKULLHEAD_CARRIED, ai_skullhead_carried);
  //ONTICK(OBJ_SKULLSTEP, ai_skullstep);
  //ONTICK(OBJ_SKULLSTEP_FOOT, ai_skullstep_foot);
  //ONTICK(OBJ_SKELETON, ai_skeleton);
  //ONTICK(OBJ_SKELETON_SHOT, ai_skeleton_shot);
  //ONTICK(OBJ_TOROKO_FRENZIED, ai_toroko_frenzied);
  //ONTICK(OBJ_TOROKO_BLOCK, ai_toroko_block);
  //ONTICK(OBJ_TOROKO_FLOWER, ai_toroko_flower);
  //ONTICK(OBJ_CURLY_BOSS, ai_curly_boss);
  //ONTICK(OBJ_CURLYBOSS_SHOT, ai_curlyboss_shot);
  //ONTICK(OBJ_PUPPY_WAG, ai_puppy_wag);
  //ONTICK(OBJ_PUPPY_BARK, ai_puppy_bark);
  //ONTICK(OBJ_PUPPY_SLEEP, ai_zzzz_spawner);
  //ONTICK(OBJ_PUPPY_RUN, ai_puppy_run);
  //ONTICK(OBJ_MA_PIGNON, ai_ma_pignon);
  //ONTICK(OBJ_MA_PIGNON_ROCK, ai_ma_pignon_rock);
  //ONTICK(OBJ_MA_PIGNON_CLONE, ai_ma_pignon_clone);
  //ONTICK(OBJ_BALROG_BOSS_RUNNING, ai_balrog_boss_running);
  ONTICK(OBJ_TOROKO_SHACK, ai_toroko_shack);
  //ONTICK(OBJ_MUSHROOM_ENEMY, ai_mushroom_enemy);
  //ONTICK(OBJ_GIANT_MUSHROOM_ENEMY, ai_mushroom_enemy);
  //ONTICK(OBJ_GRAVEKEEPER, ai_gravekeeper);
  //ONTICK(OBJ_CAGE, ai_cage);
  //ONTICK(OBJ_FIREWHIRR, ai_firewhirr);
  //ONTICK(OBJ_FIREWHIRR_SHOT, ai_firewhirr_shot);
  //ONTICK(OBJ_GAUDI_EGG, ai_gaudi_egg);
  //ONTICK(OBJ_FUZZ_CORE, ai_fuzz_core);
  //ONTICK(OBJ_FUZZ, ai_fuzz);
  //ONTICK(OBJ_BUYOBUYO_BASE, ai_buyobuyo_base);
  //ONTICK(OBJ_BUYOBUYO, ai_buyobuyo);
  //ONTICK(OBJ_BLOCK_MOVEH, ai_block_moveh);
  //ONTICK(OBJ_BLOCK_MOVEV, ai_block_movev);
  //ONTICK(OBJ_BOULDER, ai_boulder);
  //ONTICK(OBJ_CRITTER_SHOOTING_PURPLE, ai_critter_shooting_purple);
  //ONTICK(OBJ_CRITTER_SHOT, ai_generic_angled_shot);
  //ONTICK(OBJ_POOH_BLACK, ai_pooh_black);
  //ONTICK(OBJ_POOH_BLACK_BUBBLE, ai_pooh_black_bubble);
  //ONTICK(OBJ_POOH_BLACK_DYING, ai_pooh_black_dying);
  //ONTICK(OBJ_BALROG_BOSS_MISSILES, ai_balrog_boss_missiles);
  //ONTICK(OBJ_BALROG_MISSILE, ai_balrog_missile);
  //ONTICK(OBJ_GAUDI, ai_gaudi);
  //ONTICK(OBJ_GAUDI_ARMORED, ai_gaudi_armored);
  //ONTICK(OBJ_GAUDI_ARMORED_SHOT, ai_gaudi_armored_shot);
  //ONTICK(OBJ_GAUDI_FLYING, ai_gaudi_flying);
  //ONTICK(OBJ_GAUDI_FLYING_SHOT, ai_generic_angled_shot);
  //ONTICK(OBJ_GAUDI_DYING, ai_gaudi_dying);
  //ONTICK(OBJ_CRITTER_HOPPING_GREEN, ai_critter);
  //ONTICK(OBJ_BASIL, ai_basil);
  //ONTICK(OBJ_BEHEMOTH, ai_behemoth);
  //ONTICK(OBJ_BEETLE_GREEN, ai_beetle_horiz);
  //ONTICK(OBJ_BEETLE_FREEFLY, ai_beetle_freefly);
  //ONTICK(OBJ_GIANT_BEETLE, ai_giant_beetle);
  //ONTICK(OBJ_GIANT_BEETLE_SHOT, ai_generic_angled_shot);
  //ONTICK(OBJ_FORCEFIELD, ai_forcefield);
  //ONTICK(OBJ_EGG_ELEVATOR, ai_egg_elevator);
  //ONTICK(OBJ_CRITTER_HOPPING_AQUA, ai_critter);
  //ONTICK(OBJ_BEETLE_FREEFLY_2, ai_beetle_freefly);
  //ONTICK(OBJ_GIANT_BEETLE_2, ai_giant_beetle);
  //ONTICK(OBJ_DRAGON_ZOMBIE, ai_dragon_zombie);
  //ONTICK(OBJ_DRAGON_ZOMBIE_SHOT, ai_generic_angled_shot);
  //ONTICK(OBJ_FALLING_SPIKE_SMALL, ai_falling_spike_small);
  //ONTICK(OBJ_FALLING_SPIKE_LARGE, ai_falling_spike_large);
  //ONTICK(OBJ_COUNTER_BOMB, ai_counter_bomb);
  //ONTICK(OBJ_COUNTER_BOMB_NUMBER, ai_counter_bomb_number);
  //ONTICK(OBJ_NPC_IGOR, ai_npc_igor);
  //ONTICK(OBJ_BOSS_IGOR, ai_boss_igor);
  //ONTICK(OBJ_IGOR_SHOT, ai_generic_angled_shot);
  //ONTICK(OBJ_BOSS_IGOR_DEFEATED, ai_boss_igor_defeated);
  //ONTICK(OBJ_BALLOS_ROTATOR, ai_ballos_rotator);
  //ONTICK(OBJ_BALLOS_PLATFORM, ai_ballos_platform);
  //ONTICK(OBJ_UDMINI_PLATFORM, ai_udmini_platform);
  //ONTICK(OBJ_UD_PELLET, ai_ud_pellet);
  //ONTICK(OBJ_UD_SMOKE, ai_ud_smoke);
  //ONTICK(OBJ_UD_SPINNER, ai_ud_spinner);
  //ONTICK(OBJ_UD_SPINNER_TRAIL, ai_ud_spinner_trail);
  //ONTICK(OBJ_UD_BLAST, ai_ud_blast);
  //ONTICK(OBJ_BALFROG_SHOT, ai_generic_angled_shot);
  //ONTICK(OBJ_X_FISHY_MISSILE, ai_x_fishy_missile);
  //ONTICK(OBJ_X_DEFEATED, ai_x_defeated);
  //ONTICK(OBJ_MINICORE, ai_minicore);
  //ONTICK(OBJ_MINICORE_SHOT, ai_minicore_shot);
  //ONTICK(OBJ_CORE_GHOSTIE, ai_core_ghostie);
  //ONTICK(OBJ_CORE_BLAST, ai_core_blast);
  //ONTICK(OBJ_IRONH_FISHY, ai_ironh_fishy);
  //ONTICK(OBJ_IRONH_SHOT, ai_ironh_shot);
  //ONTICK(OBJ_BRICK_SPAWNER, ai_brick_spawner);
  //ONTICK(OBJ_IRONH_BRICK, ai_ironh_brick);
  //ONTICK(OBJ_IKACHAN_SPAWNER, ai_ikachan_spawner);
  //ONTICK(OBJ_IKACHAN, ai_ikachan);
  //ONTICK(OBJ_MOTION_WALL, ai_motion_wall);
  //ONTICK(OBJ_HP_LIGHTNING, ai_hp_lightning);
  //ONTICK(OBJ_OMEGA_SHOT, ai_omega_shot);
  //ONTICK(OBJ_SKY_DRAGON, ai_sky_dragon);
  //ONTICK(OBJ_SANDCROC_OSIDE, ai_sandcroc);
  //ONTICK(OBJ_NIGHT_SPIRIT, ai_night_spirit);
  //ONTICK(OBJ_NIGHT_SPIRIT_SHOT, ai_night_spirit_shot);
  //ONTICK(OBJ_HOPPY, ai_hoppy);
  //ONTICK(OBJ_PIXEL_CAT, ai_pixel_cat);
  //ONTICK(OBJ_LITTLE_FAMILY, ai_little_family);
  //ONTICK(OBJ_BALLOS_PRIEST, ai_ballos_priest);
  //ONTICK(OBJ_BALLOS_TARGET, ai_ballos_target);
  //ONTICK(OBJ_BALLOS_BONE_SPAWNER, ai_ballos_bone_spawner);
  //ONTICK(OBJ_BALLOS_BONE, ai_ballos_bone);
  //ONTICK(OBJ_BUTE_FLYING, ai_bute_flying);
  //ONTICK(OBJ_BUTE_DYING, ai_bute_dying);
  //ONTICK(OBJ_BUTE_SPAWNER, ai_bute_spawner);
  //ONTICK(OBJ_BUTE_FALLING, ai_bute_falling);
  //ONTICK(OBJ_BUTE_SWORD, ai_bute_sword);
  //ONTICK(OBJ_BUTE_ARCHER, ai_bute_archer);
  //ONTICK(OBJ_BUTE_ARROW, ai_bute_arrow);
  //ONTICK(OBJ_MESA, ai_mesa);
  //ONTICK(OBJ_MESA_BLOCK, ai_mesa_block);
  //ONTICK(OBJ_MESA_DYING, ai_bute_dying);
  //ONTICK(OBJ_DELEET, ai_deleet);
  //ONTICK(OBJ_ROLLING, ai_rolling);
  //ONTICK(OBJ_STATUE, ai_statue);
  //ONTICK(OBJ_STATUE_BASE, ai_statue_base);
  //ONTICK(OBJ_PUPPY_GHOST, ai_puppy_ghost);
  //ONTICK(OBJ_BALLOS_SKULL, ai_ballos_skull);
  //ONTICK(OBJ_BALLOS_SPIKES, ai_ballos_spikes);
  //ONTICK(OBJ_GREEN_DEVIL, ai_green_devil);
  //ONTICK(OBJ_GREEN_DEVIL_SPAWNER, ai_green_devil_spawner);
  //ONTICK(OBJ_BUTE_SWORD_RED, ai_bute_sword_red);
  //ONTICK(OBJ_BUTE_ARCHER_RED, ai_bute_archer_red);
  //ONTICK(OBJ_WALL_COLLAPSER, ai_wall_collapser);
  //ONTICK(OBJ_BALROG_BOSS_FLYING, ai_balrog_boss_flying);
  //ONTICK(OBJ_BALROG_SHOT_BOUNCE, ai_balrog_shot_bounce);
  //ONTICK(OBJ_CRITTER_FLYING, ai_critter);
  //ONTICK(OBJ_POWER_CRITTER, ai_critter);
  //ONTICK(OBJ_BAT_HANG, ai_bat_hang);
  //ONTICK(OBJ_BAT_CIRCLE, ai_bat_circle);
  //ONTICK(OBJ_JELLY, ai_jelly);
  //ONTICK(OBJ_GIANT_JELLY, ai_giant_jelly);
  //ONTICK(OBJ_MANNAN, ai_mannan);
  //ONTICK(OBJ_MANNAN_SHOT, ai_mannan_shot);
  //ONTICK(OBJ_FROG, ai_frog);
  //ONTICK(OBJ_MINIFROG, ai_frog);
  //ONTICK(OBJ_SANTAS_KEY, ai_animate2);
  //ONTICK(OBJ_HEY_SPAWNER, ai_hey_spawner);
  //ONTICK(OBJ_MOTORBIKE, ai_motorbike);
  //ONTICK(OBJ_POWERCOMP, ai_animate3);
  //ONTICK(OBJ_POWERSINE, ai_animate1);
  //ONTICK(OBJ_MALCO, ai_malco);
  //ONTICK(OBJ_MALCO_BROKEN, ai_malco_broken);
  //ONTICK(OBJ_FRENZIED_MIMIGA, ai_frenzied_mimiga);
  //ONTICK(OBJ_CRITTER_HOPPING_RED, ai_critter_hopping_red);
  //ONTICK(OBJ_LAVA_DRIP_SPAWNER, ai_lava_drip_spawner);
  //ONTICK(OBJ_LAVA_DRIP, ai_lava_drip);
  //ONTICK(OBJ_RED_BAT_SPAWNER, ai_red_bat_spawner);
  //ONTICK(OBJ_RED_BAT, ai_red_bat);
  //ONTICK(OBJ_RED_DEMON, ai_red_demon);
  //ONTICK(OBJ_RED_DEMON_SHOT, ai_droll_shot);
  //ONTICK(OBJ_PROXIMITY_PRESS_VERT, ai_proximity_press_vert);
  //ONTICK(OBJ_WATERLEVEL, ai_waterlevel);
  //ONTICK(OBJ_SHUTTER, ai_shutter);
  //ONTICK(OBJ_SHUTTER_BIG, ai_shutter);
  //ONTICK(OBJ_ALMOND_LIFT, ai_shutter);
  //ONTICK(OBJ_SHUTTER_STUCK, ai_shutter_stuck);
  //ONTICK(OBJ_ALMOND_ROBOT, ai_almond_robot);
  //ONTICK(OBJ_NULL, ai_null);
  ONTICK(OBJ_HVTRIGGER, ai_hvtrigger);
  ONTICK(OBJ_XP, ai_xp);
  ONTICK(OBJ_HEART, ai_powerup);
  ONTICK(OBJ_HEART3, ai_powerup);
  ONTICK(OBJ_MISSILE, ai_powerup);
  ONTICK(OBJ_MISSILE3, ai_powerup);
  //ONTICK(OBJ_HIDDEN_POWERUP, ai_hidden_powerup);
  //ONTICK(OBJ_DOOR, ai_door);
  //ONTICK(OBJ_LARGEDOOR, ai_largedoor);
  ONTICK(OBJ_SAVE_POINT, ai_save_point);
  ONTICK(OBJ_RECHARGE, ai_recharge);
  ONTICK(OBJ_CHEST_CLOSED, ai_chest_closed);
  ONTICK(OBJ_CHEST_OPEN, ai_chest_open);
  //ONTICK(OBJ_TELEPORTER, ai_teleporter);
  //ONTICK(OBJ_TELEPORTER_LIGHTS, ai_animate2);
  ONTICK(OBJ_COMPUTER, ai_animate4);
  ONTICK(OBJ_TERMINAL, ai_terminal);
  ONTICK(OBJ_LIFE_CAPSULE, ai_animate4);
  //ONTICK(OBJ_XP_CAPSULE, ai_xp_capsule);
  //ONTICK(OBJ_SPRINKLER, ai_sprinkler);
  //ONTICK(OBJ_WATER_DROPLET, ai_water_droplet);
  //ONTICK(OBJ_LAVA_DROPLET, ai_water_droplet);
  //ONTICK(OBJ_DROPLET_SPAWNER, ai_droplet_spawner);
  //ONTICK(OBJ_FAN_UP, ai_fan_vert);
  //ONTICK(OBJ_FAN_DOWN, ai_fan_vert);
  //ONTICK(OBJ_FAN_LEFT, ai_fan_hoz);
  //ONTICK(OBJ_FAN_RIGHT, ai_fan_hoz);
  //ONTICK(OBJ_FAN_DROPLET, ai_fan_droplet);
  //ONTICK(OBJ_PRESS, ai_press);
  ONTICK(OBJ_HIDDEN_SPARKLE, ai_animate4);
  //ONTICK(OBJ_LIGHTNING, ai_lightning);
  ONTICK(OBJ_STRAINING, ai_straining);
  //ONTICK(OBJ_BUBBLE_SPAWNER, ai_bubble_spawner);
  //ONTICK(OBJ_CHINFISH, ai_chinfish);
  ONTICK(OBJ_FIREPLACE, ai_fireplace);
  //ONTICK(OBJ_SMOKE_DROPPER, ai_smoke_dropper);
  ONTICK(OBJ_SCROLL_CONTROLLER, ai_scroll_controller);
  //ONTICK(OBJ_QUAKE, ai_quake);
  //ONTICK(OBJ_SMOKE_CLOUD, ai_smokecloud);
  //ONTICK(OBJ_MAHIN, ai_npc_mahin);
  //ONTICK(OBJ_YAMASHITA_PAVILION, ai_yamashita_pavilion);
  //ONTICK(OBJ_CHTHULU, ai_chthulu);
  ONTICK(OBJ_KAZUMA_AT_COMPUTER, ai_npc_at_computer);
  ONTICK(OBJ_SUE_AT_COMPUTER, ai_npc_at_computer);
  ONTICK(OBJ_JENKA, ai_jenka);
  ONTICK(OBJ_BLUE_ROBOT, ai_blue_robot);
  //ONTICK(OBJ_DOCTOR, ai_doctor);
  ONTICK(OBJ_TOROKO, ai_toroko);
  //ONTICK(OBJ_TOROKO_TELEPORT_IN, ai_toroko_teleport_in);
  //ONTICK(OBJ_SUE, ai_npc_sue);
  //ONTICK(OBJ_SUE_TELEPORT_IN, ai_sue_teleport_in);
  ONTICK(OBJ_KING, ai_king);
  ONTICK(OBJ_KANPACHI_FISHING, ai_kanpachi_fishing);
  //ONTICK(OBJ_PROFESSOR_BOOSTER, ai_professor_booster);
  //ONTICK(OBJ_BOOSTER_FALLING, ai_booster_falling);
  ONTICK(OBJ_NPC_PLAYER, ai_npc_player);
  //ONTICK(OBJ_PTELIN, ai_ptelin);
  //ONTICK(OBJ_PTELOUT, ai_ptelout);
  //ONTICK(OBJ_CURLY_AI, ai_curly_ai);
  //ONTICK(OBJ_CAI_GUN, ai_cai_gun);
  ONTICK(OBJ_CURLY, ai_curly);
  //ONTICK(OBJ_CURLY_CARRIED_SHOOTING, ai_curly_carried_shooting);
  //ONTICK(OBJ_CCS_GUN, ai_ccs_gun);
  //ONTICK(OBJ_BALROG, ai_balrog);
  //ONTICK(OBJ_BALROG_DROP_IN, ai_balrog_drop_in);
  //ONTICK(OBJ_BALROG_BUST_IN, ai_balrog_bust_in);
  //ONTICK(OBJ_MISERY_FLOAT, ai_misery_float);
  //ONTICK(OBJ_MISERY_STAND, ai_misery_stand);
  //ONTICK(OBJ_MISERYS_BUBBLE, ai_miserys_bubble);
  
  //onspawn
  //ONSPAWN(OBJ_MIMIGA_CAGE, onspawn_mimiga_cage);
  //ONSPAWN(OBJ_FLOWERS_PENS1, onspawn_snap_to_ground);
  //ONSPAWN(OBJ_UD_MINICORE_IDLE, onspawn_ud_minicore_idle);
  //ONSPAWN(OBJ_SPIKE_SMALL, onspawn_spike_small);
  //ONSPAWN(OBJ_YAMASHITA_FLOWERS, onspawn_set_frame_from_id2);
  //ONSPAWN(OBJ_SUE, onspawn_npc_sue);
  //ONSPAWN(OBJ_BALROG, onspawn_balrog);
  
  //ondeath
  //ONDEATH(OBJ_POLISH, ondeath_polish);
  //ONDEATH(OBJ_BALROG_BOSS_RUNNING, ondeath_balrog_boss_running);
  //ONDEATH(OBJ_BALROG_BOSS_MISSILES, ondeath_balrog_boss_missiles);
  //ONDEATH(OBJ_BALLOS_MAIN, ondeath_ballos);
  //ONDEATH(OBJ_BALFROG, ondeath_balfrog);
  //ONDEATH(OBJ_X_TARGET, ondeath_x_target);
  //ONDEATH(OBJ_X_MAINOBJECT, ondeath_x_mainobject);
  //ONDEATH(OBJ_IRONH, ondeath_ironhead);
  //ONDEATH(OBJ_OMEGA_BODY, ondeath_omega_body);
  //ONDEATH(OBJ_BALROG_BOSS_FLYING, ondeath_balrog_boss_flying);
  
  //aftermove
  //AFTERMOVE(OBJ_MISSILE_SHOT, ai_missile_shot);
  //AFTERMOVE(OBJ_SUPERMISSILE_SHOT, ai_missile_shot);
  //AFTERMOVE(OBJ_MISSILE_BOOM_SPAWNER, ai_missile_boom_spawner);
  //AFTERMOVE(OBJ_BLADE12_SHOT, aftermove_blade_l12_shot);
  //AFTERMOVE(OBJ_BLADE_SLASH, aftermove_blade_slash);
  //AFTERMOVE(OBJ_RED_CRYSTAL, aftermove_red_crystal);
  //AFTERMOVE(OBJ_MISERY_RING, aftermove_misery_ring);
  //AFTERMOVE(OBJ_SKULLHEAD_CARRIED, aftermove_skullhead_carried);
  //AFTERMOVE(OBJ_TOROKO_BLOCK, aftermove_toroko_block);
  //AFTERMOVE(OBJ_PUPPY_CARRY, aftermove_puppy_carry);
  //AFTERMOVE(OBJ_FUZZ, aftermove_fuzz);
  //AFTERMOVE(OBJ_BALLOS_ROTATOR, aftermove_ballos_rotator);
  //AFTERMOVE(OBJ_CORE_BACK, ai_core_back);
  //AFTERMOVE(OBJ_CORE_FRONT, ai_core_front);
  //AFTERMOVE(OBJ_SUE, aftermove_npc_sue);
  //AFTERMOVE(OBJ_KINGS_SWORD, aftermove_StickToLinkedActionPoint);
  //AFTERMOVE(OBJ_CAI_GUN, aftermove_cai_gun);
  //AFTERMOVE(OBJ_CAI_WATERSHIELD, aftermove_cai_watershield);
  //AFTERMOVE(OBJ_CURLY_CARRIED, aftermove_curly_carried);*/
}
