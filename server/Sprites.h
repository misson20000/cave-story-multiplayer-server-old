#ifndef SPRITES_H
#define SPRITES_H
#define MAX_SPRITES 512
#include"../siflib/sif.h"
#include"objtypes.h"
extern SIFSprite sprites[MAX_SPRITES];
bool load_sif(char const *fname);
#endif
