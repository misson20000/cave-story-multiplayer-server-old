#include<Object.h>
#include<sprites.h>
#include<Sprites.h>
#include<flags.h>
#include<Map.h>
#include<PlayerMP.h>
#include<misc.h>
#include<slope.h>
#include<effects.h>
#include<Server.h>
#include<string.h>
#include<logging.h>

ObjProp objprop[OBJ_LAST];

Object::Object(int x, int y, short id1, short id2, short type, short flags,
               Map *map, int uid) {
  memset(this, 0, sizeof(Object));
  this->sprite = objprop[type].sprite;
  this->x = x - (sprites[sprite].spawn_point.x << CSF);
  this->y = y - (sprites[sprite].spawn_point.y << CSF);
  this->id1 = id1;
  this->id2 = id2;
  this->type = type;
  this->flags = objprop[type].defaultflags | flags;
  this->map = map;
  this->uid = uid;
  this->hp = objprop[type].initial_hp;
  this->damage = objprop[type].damage;
  this->frame = 0;
}

void Object::SetType(int type) {
  Object *const &o = this;
  
  o->type = type;
  o->sprite = objprop[type].sprite;
  o->hp = objprop[type].initial_hp;
  o->damage = objprop[type].damage;
  o->frame = 0;
}

void Object::ChangeType(short type) {
  Object *const &o = this;
  int oldsprite = o->sprite;
  
  // adjust position so spawn points of old object and new object line up
  o->x >>= CSF; o->x <<= CSF;
  o->y >>= CSF; o->y <<= CSF;
  o->x += (sprites[oldsprite].spawn_point.x << CSF);
  o->y += (sprites[oldsprite].spawn_point.y << CSF);
  o->x -= (sprites[this->sprite].spawn_point.x << CSF);
  o->y -= (sprites[this->sprite].spawn_point.y << CSF);
  o->type = type;
  o->sprite = objprop[type].sprite;
  const static int flags_to_keep =					\
      (FLAG_SCRIPTONTOUCH | FLAG_SCRIPTONDEATH | FLAG_SCRIPTONACTIVATE |	\
       FLAG_APPEAR_ON_FLAGID | FLAG_DISAPPEAR_ON_FLAGID |			\
       FLAG_FACES_RIGHT);
       
  uint32_t keep = (o->flags & flags_to_keep);
  o->flags = keep;
  
  ITERATE_PLAYERS_MAP(o->map) {
    (*i)->getNetHandler()->update_object_pos(o->uid, o->x, o->y);
    (*i)->getNetHandler()->update_object_frame(o->uid, 0);
    (*i)->getNetHandler()->update_object_type(o->uid, type);
  }
}

void Object::SetDir(char dir) {
  this->dir = dir;
  ITERATE_PLAYERS_MAP(this->map) {
    (*i)->getNetHandler()->update_object_dir(uid, dir);
  }
}

void Object::DoAI() {
  if(objprop[type].ai_routines.ontick) {
    animation.updated = false;
    (*objprop[this->type].ai_routines.ontick)(this);
    
    if(animation.updated && !animation.enabled) {
      ITERATE_PLAYERS_MAP(map) {
        (*i)->getNetHandler()->animate(uid, 0, 0, 0);
      }
    }
  }
}

void Object::aicmd(int id) {
  ITERATE_PLAYERS_MAP(map) {
    (*i)->getNetHandler()->object_ai(uid, id);
  }
}

int sq(int n) {
  return n * n;
}

int Object::PlayerXDistance() {
  return abs(ClosestPlayer()->CenterX() - this->CenterX());
}

int Object::PlayerYDistance() {
  return abs(ClosestPlayer()->CenterY() - this->CenterY());
}

int Object::AboveBelow(int above, int below) {
  return (ClosestPlayer()->CenterY() > this->CenterY()) ? below : above;
}
PlayerMP *Object::ClosestPlayer() {
  PlayerMP *closest = NULL;
  int dsq = -1;
  ITERATE_PLAYERS_MAP(this->map) {
    int dsql = sq(abs((*i)->CenterX() - this->CenterX())) + sq(abs((
                 *i)->CenterY() - this->CenterY()));
                 
    if(dsq == -1 || dsql < dsq) {
      closest = *i;
    }
  }
  return closest;
}

void UpdateFrame(Object *o) {
  if(!objprop[o->type].no_update_frame) {
    ITERATE_PLAYERS_MAP(o->map) {
      (*i)->getNetHandler()->update_object_frame(o->uid, o->frame);
    }
  }
}

void UpdateDir(Object *o) {
  ITERATE_PLAYERS_MAP(o->map) {
    (*i)->getNetHandler()->update_object_dir(o->uid, o->dir);
  }
}

void UpdateSprite(Object *o) {
  ITERATE_PLAYERS_MAP(o->map) {
    (*i)->getNetHandler()->set_sprite(o->uid, o->sprite);
  }
}

void Object::SendUpdates(uint16_t mask) {
  if(mask & UPDATE_MASK_POSITION && !(objprop[type].no_update_pos)) {
    if(x >> 12 != fx >> 12 || y >> 12 != fy >> 12) { //Do full update
      ITERATE_PLAYERS_MAP(map) {
	(*i)->getNetHandler()->update_object_pos(uid, x, y);
      }
      fx = lx = x;
      fy = ly = y;
    } else if(x >> CSF != lx >> CSF || y >> CSF != ly >> CSF) {
      ITERATE_PLAYERS_MAP(map) {
	(*i)->getNetHandler()->do_delta(uid, x >> CSF, y >> CSF, lx >> CSF, ly >> CSF);
      }
      lx = x;
      ly = y;
    }
  }
  
  if(mask & UPDATE_MASK_FRAME) {
    if(!animation.enabled && frame != lframe) {
      UpdateFrame(this);
      lframe = frame;
    }
  }
  
  if(mask & UPDATE_MASK_DIR) {
    if(dir != ldir) {
      UpdateDir(this);
      ldir = dir;
    }
  }

  if(mask & UPDATE_MASK_SPRITE) {
    UpdateSprite(this);
  }
}

// tries to move the object in the X direction by the given amount.
// returns nonzero if the object was blocked.
bool Object::apply_xinertia(int inertia) {
  Object *const &o = this;
  
  if(inertia == 0)
  { return 0; }
  
  if(o->flags & FLAG_IGNORE_SOLID) {
    o->x += inertia;
    return 0;
  }
  
  // only apply inertia one pixel at a time so we have
  // proper hit detection--prevents objects traveling at
  // high speed from becoming embedded in walls
  if(inertia > 0) {
    while(inertia > (1<<CSF)) {
      if(movehandleslope(o, (1<<CSF))) { return 1; }
      
      inertia -= (1<<CSF);
      
      o->UpdateBlockStates(RIGHTMASK);
    }
  } else if(inertia < 0) {
    while(inertia < -(1<<CSF)) {
      if(movehandleslope(o, -(1<<CSF))) { return 1; }
      
      inertia += (1<<CSF);
      
      o->UpdateBlockStates(LEFTMASK);
    }
  }
  
  // apply any remaining inertia
  if(inertia)
  { movehandleslope(o, inertia); }
  
  return 0;
}

// tries to move the object in the Y direction by the given amount.
// returns nonzero if the object was blocked.
bool Object::apply_yinertia(int inertia) {
  Object *const &o = this;
  
  if(inertia == 0)
  { return 0; }
  
  if(o->flags & FLAG_IGNORE_SOLID) {
    o->y += inertia;
    return 0;
  }
  
  // only apply inertia one pixel at a time so we have
  // proper hit detection--prevents objects traveling at
  // high speed from becoming embedded in walls
  if(inertia > 0) {
    if(o->blockd) { return 1; }
    
    while(inertia > (1<<CSF)) {
      o->y += (1<<CSF);
      inertia -= (1<<CSF);
      
      o->UpdateBlockStates(DOWNMASK);
      
      if(o->blockd) { return 1; }
    }
  } else if(inertia < 0) {
    if(o->blocku) { return 1; }
    
    while(inertia < -(1<<CSF)) {
      o->y -= (1<<CSF);
      inertia += (1<<CSF);
      
      o->UpdateBlockStates(UPMASK);
      
      if(o->blocku) { return 1; }
    }
  }
  
  // apply any remaining inertia
  if(inertia)
  { o->y += inertia; }
  
  return 0;
}

void Object::SnapToGround() {
  Object *const &o = this;
  
  uint32_t flags = o->flags;
  o->flags &= ~FLAG_IGNORE_SOLID;
  
  UpdateBlockStates(DOWNMASK);
  apply_yinertia(SCREEN_HEIGHT << CSF);
  
  o->flags = flags;
  o->blockd = true;
}

void Object::UpdateBlockStates(uint8_t updatemask) {
  Object *const &o = this;
  SIFSprite *sprite = Sprite();
  int mask = GetBlockingType();
  
  if(updatemask & LEFTMASK) {
    o->blockl = CheckAttribute(&sprite->block_l, mask);
    
    // for objects which don't follow slope, have them see the slope as a wall so they
    // won't just go right through it (looks really weird)
    if(!(o->nxflags & NXFLAG_FOLLOW_SLOPE)) {
      if(!o->blockl)
      { o->blockl = IsSlopeAtPointList(o, &sprite->block_l); }
    }
  }
  
  if(updatemask & RIGHTMASK) {
    o->blockr = CheckAttribute(&sprite->block_r, mask);
    
    // for objects which don't follow slope, have them see the slope as a wall so they
    // won't just go right through it (looks really weird).
    if(!(o->nxflags & NXFLAG_FOLLOW_SLOPE)) {
      if(!o->blockr)
      { o->blockr = IsSlopeAtPointList(o, &sprite->block_r); }
    }
  }
  
  if(updatemask & UPMASK) {
    o->blocku = CheckAttribute(&sprite->block_u, mask);
    
    if(!o->blocku) { o->blocku = CheckBoppedHeadOnSlope(o) ? 1 : 0; }
  }
  
  if(updatemask & DOWNMASK) {
    o->blockd = CheckAttribute(&sprite->block_d, mask);
    
    if(!o->blockd) { o->blockd = CheckStandOnSlope(o) ? 1 : 0; }
  }
  
  // have player be blocked by objects with FLAG_SOLID_BRICK set
  //if (o == player)
  //  o->SetBlockForSolidBrick(updatemask);
}

bool Object::CheckAttribute(const Point *pointlist, int npoints,
                            uint32_t attrmask, int *tile_x, int *tile_y) {
  int x, y, xoff, yoff;
  
  xoff = (this->x >> CSF);
  yoff = (this->y >> CSF);
  
  for(int i=0; i<npoints; i++) {
    x = (xoff + pointlist[i].x) / TILE_W;
    y = (yoff + pointlist[i].y) / TILE_H;
    
    if(x >= 0 && y >= 0 && x < map->xsize && y < map->ysize) {
      if((map->tileattr[map->tiles[x][y]] & attrmask) != 0) {
        if(tile_x) { *tile_x = x; }
        
        if(tile_y) { *tile_y = y; }
        
        return true;
      }
    }
  }
  
  return false;
}

// treats each point in pointlist as an offset within the object, and returns
// true if any of the points intersect with object o2's solidbox.
bool Object::CheckSolidIntersect(Object *other, const Point *pointlist,
                                 int npoints) {
  int x, y;
  int ox, oy, o2x, o2y;
  SIFSprite *s2 = other->Sprite();
  
  ox = (this->x >> CSF);
  oy = (this->y >> CSF);
  o2x = (other->x >> CSF);
  o2y = (other->y >> CSF);
  
  for(int i=0; i<npoints; i++) {
    x = ox + pointlist[i].x;
    y = oy + pointlist[i].y;
    
    if(x >= (o2x + s2->solidbox.x1) && x <= (o2x + s2->solidbox.x2)) {
      if(y >= (o2y + s2->solidbox.y1) && y <= (o2y + s2->solidbox.y2)) {
        return true;
      }
    }
  }
  
  return false;
}

// returns true if the solidity boxes of the two given objects are touching
bool solidhitdetect(Object *o1, Object *o2) {
  SIFSprite *s1, *s2;
  int32_t rect1x1, rect1y1, rect1x2, rect1y2;
  int32_t rect2x1, rect2y1, rect2x2, rect2y2;
  
  // get the sprites used by the two objects
  s1 = o1->Sprite();
  s2 = o2->Sprite();
  
  // get the bounding rectangle of the first object
  rect1x1 = o1->x + (s1->solidbox.x1 << CSF);
  rect1x2 = o1->x + (s1->solidbox.x2 << CSF);
  rect1y1 = o1->y + (s1->solidbox.y1 << CSF);
  rect1y2 = o1->y + (s1->solidbox.y2 << CSF);
  
  // get the bounding rectangle of the second object
  rect2x1 = o2->x + (s2->solidbox.x1 << CSF);
  rect2x2 = o2->x + (s2->solidbox.x2 << CSF);
  rect2y1 = o2->y + (s2->solidbox.y1 << CSF);
  rect2y2 = o2->y + (s2->solidbox.y2 << CSF);
  
  // find out if the rectangles overlap
  if((rect1x1 < rect2x1) && (rect1x2 < rect2x1)) { return false; }
  
  if((rect1x1 > rect2x2) && (rect1x2 > rect2x2)) { return false; }
  
  if((rect1y1 < rect2y1) && (rect1y2 < rect2y1)) { return false; }
  
  if((rect1y1 > rect2y2) && (rect1y2 > rect2y2)) { return false; }
  
  return true;
}


// returns true if the bounding boxes of the two given objects are touching
bool hitdetect(Object *o1, Object *o2) {
  SIFSprite *s1, *s2;
  int32_t rect1x1, rect1y1, rect1x2, rect1y2;
  int32_t rect2x1, rect2y1, rect2x2, rect2y2;
  
  // get the sprites used by the two objects
  s1 = o1->Sprite();
  s2 = o2->Sprite();
  
  // get the bounding rectangle of the first object
  rect1x1 = o1->x + (s1->bbox.x1 << CSF);
  rect1x2 = o1->x + (s1->bbox.x2 << CSF);
  rect1y1 = o1->y + (s1->bbox.y1 << CSF);
  rect1y2 = o1->y + (s1->bbox.y2 << CSF);
  
  // get the bounding rectangle of the second object
  rect2x1 = o2->x + (s2->bbox.x1 << CSF);
  rect2x2 = o2->x + (s2->bbox.x2 << CSF);
  rect2y1 = o2->y + (s2->bbox.y1 << CSF);
  rect2y2 = o2->y + (s2->bbox.y2 << CSF);
  
  // find out if the rectangles overlap
  if((rect1x1 < rect2x1) && (rect1x2 < rect2x1)) { return false; }
  
  if((rect1x1 > rect2x2) && (rect1x2 > rect2x2)) { return false; }
  
  if((rect1y1 < rect2y1) && (rect1y2 < rect2y1)) { return false; }
  
  if((rect1y1 > rect2y2) && (rect1y2 > rect2y2)) { return false; }
  
  return true;
}

// subfunction of HandleContactDamage. On entry, we assume that the player
// is in contact with this object, and that the object is trying to deal
// damage to him.
// returns the type of attack:
//	- UP	a top attack (player hit top of object)
//	- LEFT	rear attack, player to left
//	- RIGHT	rear attack, player to right
//	- -1	head-on or bottom attack
int Object::GetAttackDirection(PlayerMP *player) {
  Object *const &o = this;
  const int VARIANCE = (5 << CSF);
  
  if(player->riding == o)
  { return UP; }
  
  if(player->Bottom() <= (o->Top() + VARIANCE))
  { return UP; }
  
  // (added for X treads) if the object is moving, then the "front"
  // for purposes of this flag is the direction it's moving in.
  // if it's still, the "front" is the actual direction it's facing.
  int rtdir = o->dir;
  
  if(o->xinertia > 0) { rtdir = RIGHT; }
  
  if(o->xinertia < 0) { rtdir = LEFT; }
  
  if(rtdir == RIGHT) {
    if(player->Right() <= (o->Left() + VARIANCE))
    { return RIGHT; }
  } else if(rtdir ==
            LEFT) {  // the double check makes sense, what if o->dir was UP or DOWN
    if(player->Left() >= (o->Right() - VARIANCE))
    { return LEFT; }
  }
  
  return -1;
}


// deals contact damage to player of o->damage, if applicable.
void Object::DealContactDamage(PlayerMP *player) {
  Object *const &o = this;
  
  // no contact damage to player while scripts running
  if(player->script.running || player->getNetHandler()->v_locked_keys)
  { return; }
  
  if(!(o->flags & FLAG_NOREARTOPATTACK)) {
    player->hurt(o->damage);
    return;
  }
  
  // else, the no rear/top attack flag is set, so only
  // frontal or bottom contact are harmful to the player
  switch(o->GetAttackDirection(player)) {
  case -1:	// head-on
    player->hurt(o->damage);
    break;
    
  case LEFT:	// rear attack, p to left
    if(player->xinertia > -0x100)
    { player->xinertia = -0x100; }
    
    break;
    
  case RIGHT:	// rear attack, p to right
    if(player->xinertia < 0x100)
    { player->xinertia = 0x100; }
    
    break;
  }
}

int Object::GetBlockingType() {
  Object *const &o = this;
  
  if(o->type >= OBJ_SHOTS_START &&		\
      o->type <= OBJ_SHOTS_END) {
    // Bubbler L1 can't pass tile 44.
    if(o->type == OBJ_BUBBLER12_SHOT && o->shot.level == 0)
    { return (TA_SOLID_SHOT | TA_SOLID_NPC); }
    
    return TA_SOLID_SHOT;
  }
  
  if(o->flags & FLAG_IGNORETILE44)
  { return TA_SOLID_PLAYER; }
  
  return TA_SOLID_NPC;
}

// the most important thing it does is apply x/y inertia to the objects.
void Object::PhysicsSim(void) {
  Object *o = this;
  o->UpdateBlockStates(ALLDIRMASK);
  int xinertia, yinertia;
  
  if(o->uid >= 256 && !o->deleted) {  // player is moved in PDoPhysics
    if(!(o->flags & FLAG_IGNORE_SOLID) &&				\
        !(o->nxflags & NXFLAG_NO_RESET_YINERTIA)) {
      if(o->blockd && o->yinertia > 0) { o->yinertia = 0; }
      
      if(o->blocku && o->yinertia < 0) { o->yinertia = 0; }
    }
    
    // apply inertia to X,Y position
    xinertia = o->xinertia;
    yinertia = o->yinertia;
    
    if(o->shaketime) {
      if(o->nxflags & NXFLAG_SLOW_X_WHEN_HURT) { xinertia >>= 1; }
      
      if(o->nxflags & NXFLAG_SLOW_Y_WHEN_HURT) { yinertia >>= 1; }
    }
    
    o->apply_xinertia(xinertia);
    o->apply_yinertia(yinertia);
    
    // flag_solid_brick objects push player as they move
    ITERATE_PLAYERS_MAP(o->map) {
      if(o->flags & FLAG_SOLID_BRICK) {
        o->PushPlayerOutOfWay(xinertia, yinertia, *i);
      } else if(o->damage > 0) {
        // have enemies hurt you when you touch them
        // (solid-brick objects do this in PHandleSolidBrickObjects)
        
        if(hitdetect(o, *i))
        { o->DealContactDamage(*i); }
      }
    }
  }
}

// handles a moving object with "FLAG_SOLID_BRICK" set
// pushing the player as it moves.
void Object::PushPlayerOutOfWay(int xinertia, int yinertia, PlayerMP *player) {
  Object *const &o = this;
  
  if(xinertia) {
    // give a bit of a gap where they must be--i.e. don't push them if they're right
    // at the top or the bottom of the brick: needed when he rides it and falls off, then it
    // turns around and touches him again. in that case what we actually want to do is push him
    // to the top, not push him side-to-side.
    if((player->SolidBottom() - (2<<CSF)) > o->SolidTop() &&		\
        (player->SolidTop() + (2<<CSF)) < o->SolidBottom()) {
      if(xinertia > 0 && player->SolidRight() > o->SolidRight()
          && solidhitdetect(o, player)) {	// pushing player right
        if(player->blockr) {	// squish!
          player->hurt(o->smushdamage);
        } else {
          // align player's blockl grid with our right side
          player->x = o->SolidRight() - (sprites[player->sprite].block_l[0].x << CSF);
          
          // get player a xinertia equal to our own. You can see this
          // with the moving blocks in Labyrinth H.
          player->xinertia = xinertia;
          player->x += -player->xinertia;
        }
      } else if(xinertia < 0 && player->SolidLeft() < o->SolidLeft()
                && solidhitdetect(o, player)) { // pushing player left
        if(player->blockl) {  // squish!
          player->hurt(o->smushdamage);
        } else {
          // align player's blockr grid with our left side
          player->x = o->SolidLeft() - (sprites[player->sprite].block_r[0].x << CSF);
          
          // get player a xinertia equal to our own. You can see this
          // with the moving blocks in Labyrinth H.
          player->xinertia = xinertia;
          player->x += -player->xinertia;
        }
      }
    }
  }
  
  if(yinertia < 0) {
    if(player->blocku && player->riding == o)	// smushed into ceiling!
    { player->hurt(o->smushdamage); }
  } else if(yinertia > 0) {  // object heading downwards?
    // player riding object down
    if(player->riding == o) {
      if(player->yinertia >= 0) {  // don't do this if he's trying to jump away
        // align player's blockd grid with our top side so player
        // doesn't perpetually fall.
        player->y = o->SolidTop() - (sprites[player->sprite].block_d[0].y << CSF);
      }
    } else if(player->Top() >= o->CenterY()
              && solidhitdetect(o, player)) {  // underneath object
      // push him down if he's underneath us and we're going faster than he is.
      if(yinertia >= player->yinertia) {
        if(player->blockd)		// squished into floor!
        { player->hurt(o->smushdamage); }
        
        // align his blocku grid with our bottom side
        player->y = o->SolidBottom() - (sprites[player->sprite].block_u[0].y << CSF);
      }
    }
  }
}

void Object::NoAnimation() {
  animation.enabled = false;
  ITERATE_PLAYERS_MAP(map) {
    (*i)->getNetHandler()->animate(uid, 0, 0, 0);
  }
}

void Object::Animate(int speed, int first, int last) {
  if(animation.enabled
      && animation.speed == speed
      && animation.first == first
      && animation.last  == last) {
    return;
  }
  
  animation.updated = true;
  animation.enabled = true;
  animation.speed = speed;
  animation.first = first;
  animation.last = last;
  
  ITERATE_PLAYERS_MAP(map) {
    (*i)->getNetHandler()->animate(uid, speed, first, last);
  }
}

void Object::DealDamage(int dmg, Object *shot) {
  Object *const &o = this;
  
  if(o->flags & FLAG_INVULNERABLE)
  { return; }
  
  o->hp -= dmg;
  
  if(o->flags & FLAG_SHOW_FLOATTEXT) {
    ITERATE_PLAYERS_MAP(o->map) {
      (*i)->getNetHandler()->deal_damage(o->uid, dmg);
    }
  }
  
  if(o->hp > 0) {
    if(o->shaketime < objprop[o->type].shaketime - 2) {
      o->shaketime = objprop[o->type].shaketime;
      
      if(objprop[o->type].hurt_sound) {
        ITERATE_PLAYERS_MAP(o->map) {
          (*i)->getNetHandler()->sound(objprop[o->type].hurt_sound);
        }
      }
      
      if(shot) {
        ITERATE_PLAYERS_MAP(o->map) {
          (*i)->getNetHandler()->effect(shot->CenterX(), shot->CenterY(),
                                        EFFECT_BLOODSPLATTER);
        }
      }
    }
  } else {
    o->Kill();
  }
}

void Object::Kill() {
  hp = 0;
  flags &= ~FLAG_SHOOTABLE;
  
  if(flags & FLAG_SCRIPTONDEATH) {
    nyi("Object::Kill StartScript");
  } else {
    map->SmokeClouds(this, objprop[type].death_smoke_amt, 8, 8);
    ITERATE_PLAYERS_MAP(map) {
      (*i)->getNetHandler()->effect(CenterX(), CenterY(), EFFECT_BOOMFLASH);
    }
    
    if(objprop[type].death_sound) {
      ITERATE_PLAYERS_MAP(map) {
        (*i)->getNetHandler()->sound(objprop[type].death_sound);
      }
    }
    
    if(objprop[type].ai_routines.ondeath) {
      nyi("Object::Kill OnDeath");
    } else {
      SpawnPowerups();
      Delete();
    }
  }
}

void Object::SpawnPowerups() {
  int objectType, bonusType;
  if(!objprop[type].xponkill) { return; }
  bonusType = random(1, 5);
  if(bonusType >= 3) {
    SpawnXP(objprop[type].xponkill);
    return;
  }
  if(bonusType == 2) { objectType = OBJ_MISSILE; }
  else               { objectType = OBJ_HEART;   }

  if(objprop[type].xponkill > 6) {
    if(objectType == OBJ_HEART) { objectType = OBJ_HEART3;   }
    else                        { objectType = OBJ_MISSILE3; }
  }

  Object *powerup = map->CreateObject(CenterX(), CenterY(), objectType);
  powerup->x -= (powerup->Width() / 2);
  powerup->y -= (powerup->Height() / 2);

  powerup->state = 1;
}

void Object::SpawnXP(int amt) {
  int x = CenterX();
  int y = CenterY();

  while(amt > 0) {
    Object *xp = map->CreateObject(x, y, OBJ_XP);
    xp->xinertia = random(-0x200, 0x200);

    if(amt >= XP_LARGE_AMT) {
      xp->sprite = SPR_XP_LARGE;
      amt -= XP_LARGE_AMT;
    } else if(amt >= XP_MED_AMT) {
      xp->sprite = SPR_XP_MED;
      amt -= XP_MED_AMT;
    } else {
      xp->sprite = SPR_XP_SMALL;
      amt -= XP_SMALL_AMT;
    }
    
    // center the sprite at the center of the object
    xp->x -= (xp->Width() / 2);
    xp->y -= (xp->Height() / 2);
    
    xp->UpdateBlockStates(ALLDIRMASK);
  }
}

void Object::Delete() {
  deleted = true;
  server.flags[id1] = true;
  ITERATE_PLAYERS_MAP(map) {
    (*i)->getNetHandler()->delete_object(uid);
  }
}
