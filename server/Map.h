#ifndef MAP_H
#define MAP_H

#include<stdio.h>
#include<maplist.h>
#include<Object.h>
#include<Tsc.h>
#include<list>

class PlayerMP;

#define TILE_W 16
#define TILE_H 16
#define ITERATE_PLAYERS_MAP(map) for(std::list<PlayerMP*>::iterator i = map->players.begin(); i != map->players.end(); i++)
#define FOREACH_OBJECT(map) for(std::list<Object*>::iterator i = map->objects.begin(); i != map->objects.end(); i++)
#define TA_SOLID_PLAYER		0x00001			// solid to player
#define TA_SOLID_NPC		0x00002			// solid to npc's, enemies and enemy shots
#define TA_SOLID_SHOT		0x00004			// solid to player's shots
#define TA_SOLID			(TA_SOLID_PLAYER | TA_SOLID_SHOT | TA_SOLID_NPC)
#define TA_HURTS_PLAYER		0x00010			// this tile hurts the player -10hp
#define TA_FOREGROUND		0x00020			// tile is drawn in front of sprites
#define TA_DESTROYABLE		0x00040			// tile is destroyable if player shoots it
#define TA_WATER			0x00080			// tile is water/underwater

#define TA_CURRENT			0x00100			// blows player (tilecode checked to see which direction)
#define TA_SLOPE			0x00200			// is a slope (the tilecode is checked to see what kind)



struct MapData {
  char filename[32];
  char tileset[32];
  char bossNo;
  char caption[32];
};

class PlayerMP;

class Map {
public:
  Map(int no);
  ~Map();
  void playerEnter(PlayerMP *p);
  void playerLeft(PlayerMP *p);
  bool removePlayers();
  Object *CreateObject(int x, int y, int type, int xinertia = 0, int yinertia = 0,
                       int dir = 0, Object *linkedobject = NULL,
		       int flag = 0, int evt = 0, int uid = -1, int flags = -1);
  void SmokeClouds(Object *o, int nclouds, int rangex=0, int rangey=0, Object *push_behind=NULL);
  static int LoadData(char *mapdata);
  static void DeleteData();
  void DoAI();
  int id;
  std::list<PlayerMP *> players;
  std::list<Object *> objects;
  Object *ID2Lookup[65536];
  Tsc tsc;
  short xsize;
  short ysize;
  unsigned int tileattr[256];
  unsigned char tilecode[256];
  uint8_t **tiles;
  bool loaded;

  int scrolltype;

  int tsc_sync_no;
  SynchronizedScriptInstance tsc_sync;
private:
  static unsigned int tilekey[256];
  int load_pxa(char *fnamepxa);
  int load_pxm(char *fnamepxm);
  int load_pxe(char *fnamepxe);
  int maxuid;
};

#endif
