#ifndef MISC_H
#define MISC_H

#ifndef _DIRECTIONS_DEFINED
#define _DIRECTIONS_DEFINED
enum Directions {
  RIGHT	= 0,
  LEFT	= 1,
  UP		= 2,
  DOWN	= 3,
  CENTER	= 5
};
#endif

#define RIGHTMASK		0x01
#define LEFTMASK		0x02
#define UPMASK			0x04
#define DOWNMASK		0x08
#define ALLDIRMASK		(LEFTMASK | RIGHTMASK | UPMASK | DOWNMASK)

#define SCREEN_WIDTH		320
#define SCREEN_HEIGHT		240

#define SWAP(a, b) {				\
    int tmp = a;				\
    a = b;					\
    b = tmp;					\
  }

int random(int min, int max);

#define XP_SMALL_AMT			1
#define XP_MED_AMT			5
#define XP_LARGE_AMT			20

#endif
