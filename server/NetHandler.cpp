#include<NetHandler.h>
#include<PlayerMP.h>
#include<Server.h>
#include<stdio.h>
#include<effects.h>
#include<flags.h>
#include<string.h>

void handle_update_pos(PlayerMP *player, NetStream *stream) {
  int x = stream->readInt();
  int y = stream->readInt();
  Map *m = player->map;
  player->x = x;
  player->y = y;
  ITERATE_PLAYERS_MAP(m) {
    if(*i != player) {
      (*i)->getNetHandler()->update_object_pos(player->uid, x, y);
    }
  }
}

void handle_update_frame(PlayerMP *player, NetStream *stream) {
  int frame = stream->readInt();
  Map *m = player->map;
  player->frame = frame;
  ITERATE_PLAYERS_MAP(m) {
    if(*i != player) {
      (*i)->getNetHandler()->update_object_frame(player->uid, frame);
    }
  }
}

void handle_update_dir(PlayerMP *player, NetStream *stream) {
  int dir = stream->readByte();
  player->dir = dir;
  Map *m = player->map;
  ITERATE_PLAYERS_MAP(m) {
    if(*i != player) {
      (*i)->getNetHandler()->update_object_dir(player->uid, dir);
    }
  }
}

bool RunScriptAtLocation(PlayerMP *p, int x, int y) {
  FOREACH_OBJECT(p->map) {
    Object *o = *i;
    
    if(o->flags & FLAG_SCRIPTONACTIVATE) {
      if(x >= o->Left() && x <= o->Right() &&
          y >= o->Top()  && y <= o->Bottom()) {
        p->map->tsc.StartScript(&p->script, o->id2);
        return true;
      }
    }
  }
  return false;
}

bool RunScriptAtX(PlayerMP *p, int x) {
  if(RunScriptAtLocation(p, x, p->y + (8 << CSF)) ||	   \
      RunScriptAtLocation(p, x, p->y + (14 << CSF)) ||	   \
      RunScriptAtLocation(p, x, p->y + (2 << CSF))) {
    return true;
  }
  
  return false;
}

void handle_activate(PlayerMP *player, NetStream *stream) {
  if(RunScriptAtX(player, player->CenterX())) {
    return;
  }
  
  if(player->dir == 1) {
    if(RunScriptAtX(player, player->Right())
        || RunScriptAtX(player, player->Left())) {
      return;
    }
  } else {
    if(RunScriptAtX(player, player->Left())
        || RunScriptAtX(player, player->Right())) {
      return;
    }
  }
  
  // e.g. Plantation Rocket
  if(player->riding && (player->riding->flags & FLAG_SCRIPTONACTIVATE)) {
    player->map->tsc.StartScript(&player->script, player->riding->id2);
    return;
  }
  
  Map *m = player->map;
  ITERATE_PLAYERS_MAP(m) {
    (*i)->getNetHandler()->effect(player->CenterX(), player->CenterY(),
                                  EFFECT_QMARK);
  }
}

void handle_nod(PlayerMP *p, NetStream *stream) {
  p->getNetHandler()->release_nod();
}

void handle_textbox_busy(PlayerMP *p, NetStream *stream) {
  p->getNetHandler()->v_textbox_busy = stream->readByte();
}

void handle_ynj_result(PlayerMP *p, NetStream *stream) {
  p->getNetHandler()->ynj_result(stream->readByte());
}

void handle_fire_pressed(PlayerMP *p, NetStream *stream) {
  p->firekey = true;
}

void handle_fire_released(PlayerMP *p, NetStream *stream) {
  p->firekey = false;
}

void handle_change_weapon(PlayerMP *p, NetStream *stream) {
  p->curWeapon = stream->readByte();
}

void handle_update_look(PlayerMP *p, NetStream *stream) {
  p->look = stream->readByte();
}

typedef void(*PacketHandler)(PlayerMP *player, NetStream *stream);
PacketHandler handlers[] = {
  NULL,
  NULL,
  handle_update_pos,
  handle_update_frame,
  handle_update_dir,
  handle_activate,
  handle_nod,
  handle_textbox_busy,
  handle_ynj_result,
  handle_fire_pressed,
  handle_fire_released,
  handle_change_weapon,
  handle_update_look
};

void disconnect(void *player) {
  server.disconnectPlayer((PlayerMP *) player);
}

NetHandler::NetHandler(NetStream *str, PlayerMP *play) {
  stream = str;
  player = play;
  char hello[6];
  stream->readBytes(hello, 5);
  hello[5] = 0;
  
  //Notify us upon stream termination -- we need to disconnect the player
  str->hooks.disconnect = disconnect;
  str->hooks.disconnect_data = play;
  
  v_ynj_ready = false;
  v_ynj_result = false;
  v_textbox_busy = false;
  v_textbox_visible = false;
  v_nod_release = false;
  
  if(strcmp(hello, "HELLO")) {
    stream->writeBytes("Bad identification text", 23);
    stream->closeSocket();
    server.disconnectPlayer(player);
    return;
  }
}

void NetHandler::handshake() {
  stream->writeByte(1);
  stream->writeByte(player->which);
}

void NetHandler::change_map(int no, int x, int y) {
  stream->writeByte(2);
  stream->writeByte(no);
  stream->writeInt(x);
  stream->writeInt(y);
  printf("Sent chmap [%d](%d,%d)\n", no, x, y);
}

void NetHandler::spawn(int x, int y, int maxhp, int hp) {
  stream->writeByte(3);
  printf("Sent spawn\n");
  //stream->writeInt(x);
  //stream->writeInt(y);
  stream->writeByte(maxhp);
  stream->writeByte(hp);
  l_hp = hp; //Client knows about this now. No need to resend
  l_maxhp = maxhp;
}

void NetHandler::clear_objects() {
  stream->writeByte(4);
}

void NetHandler::send_object(Object *obj) {
  stream->writeByte(5);
  obj->lx = obj->x;
  obj->ly = obj->y;
  stream->writeInt(obj->x);
  stream->writeInt(obj->y);
  stream->writeShort(obj->id1);
  stream->writeShort(obj->id2);
  stream->writeShort(obj->type);
  stream->writeShort(obj->flags);
  stream->writeShort(obj->uid);
}

void NetHandler::kill_object(Object *obj) {
  stream->writeByte(6);
  stream->writeShort(obj->uid);
}

void NetHandler::update_object_pos(short uid, int x, int y) {
  stream->writeByte(7);
  stream->writeShort(uid);
  stream->writeInt(x);
  stream->writeInt(y);
}

void NetHandler::update_object_frame(short uid, int frame) {
  stream->writeByte(8);
  stream->writeShort(uid);
  stream->writeInt(frame);
}

void NetHandler::update_object_dir(short uid, uint8_t dir) {
  stream->writeByte(9);
  stream->writeShort(uid);
  stream->writeByte(dir);
}

void NetHandler::effect(int x, int y, short type) {
  stream->writeByte(10);
  stream->writeInt(x);
  stream->writeInt(y);
  stream->writeShort(type);
}

void NetHandler::textbox_text(char *txt) {
  stream->writeByte(12);
  stream->writeByte(3);
  stream->writeInt(txt[0]);
  stream->writeBytes(txt+1, txt[0]);
}

void NetHandler::smoke_clouds(int x, int y, int nclouds, int rangex, int rangey, Object *push_behind) {
  stream->writeByte(36);
  stream->writeInt(x);
  stream->writeInt(y);
  stream->writeInt(nclouds);
  stream->writeInt(rangex);
  stream->writeInt(rangey);
  stream->writeShort(push_behind? push_behind->uid : 0);
}

void send_wpn(NetStream *stream, Weapon *wep) {
  stream->writeByte(wep->hasWeapon);
  stream->writeByte(wep->xp);
  stream->writeByte(wep->level);
  stream->writeShort(wep->ammo);  
  stream->writeShort(wep->maxammo);
}

void NetHandler::update_weapons(Weapon *ptr, int cw) {
  stream->writeByte(38);
  for(int i = 0; i < WPN_COUNT; i++) {
    send_wpn(stream, ptr+i);
  }
  stream->writeByte(cw);
}

void NetHandler::handlePackets() {
  int c = 0;
  
  while(!stream->isClosed() && stream->bytesAvailable() > 0) {
    if(c > 5) {
      break;
    }
    
    int id = stream->readByte();
    handlers[id](player, stream);
    c++;
  }
  
  if(player->hp != l_hp || player->maxHealth != l_maxhp) {
    update_hp(player->hp, player->maxHealth);
  }

  stream->flush();
  
  if(stream->isClosed()) {
    server.disconnectPlayer(player);
  }
}

void NetHandler::ynj_result(bool result) {
  v_ynj_ready = true;
  v_ynj_result = result;
}

void NetHandler::do_delta_x(short uid, int x, int y, int lx) {
  if(abs(x - lx) > 65534/2) { // Ehh.. that's a lot of pixels to be moving
    //Don't bother with delta that big
    update_pos_no_csf(uid, x, y);
  } else if(abs(x - lx) > 254/2) {
    delta_pos_x_short(uid, x - lx);
  } else {
    delta_pos_x_byte(uid, x - lx);
  }
}

void NetHandler::do_delta_y(short uid, int x, int y, int ly) {
  if(abs(y - ly) > 65534/2) { // Ehh.. that's a lot of pixels to be moving
    //Don't bother with delta that big
    update_pos_no_csf(uid, x, y);
  } else if(abs(y - ly) > 254/2) {
    delta_pos_y_short(uid, y - ly);
  } else {
    delta_pos_y_byte(uid, y - ly);
  }
}

void NetHandler::do_delta(short uid, int x, int y, int lx, int ly) {
  if(x != lx && y == ly) {
    do_delta_x(uid, x, y, lx);
  } else if(x == lx && y != ly) {
    do_delta_y(uid, x, y, ly);
  } else {
    if(abs(x - lx) > 65534/2) {
      update_pos_no_csf(uid, x, y);
    } else if(abs(x - lx) > 254/2) {
      delta_pos_xy_short(uid, x - lx, y - ly);
    } else {
      delta_pos_xy_byte(uid, x - lx, y - ly);
    }
  }
}

NetHandler::~NetHandler() {
  if(!stream->isClosed()) {
    stream->closeSocket();
  }
  
  delete stream;
}
