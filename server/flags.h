#define FLAG_SOLID_MUSHY			0x0001	// object blocks player but is a little "mushy" (normal solid state for enemies)
#define FLAG_IGNORETILE44			0x0002
#define FLAG_INVULNERABLE			0x0004
#define FLAG_IGNORE_SOLID			0x0008
#define FLAG_BOUNCY					0x0010	// when SOLID_BRICK also set, acts like a mini trampoline
#define FLAG_SHOOTABLE				0x0020
#define FLAG_SOLID_BRICK			0x0040	// object's entire bbox is rock-solid, just like a solid tile
#define FLAG_NOREARTOPATTACK		0x0080
#define FLAG_SCRIPTONTOUCH			0x0100
#define FLAG_SCRIPTONDEATH			0x0200
#define FLAG_DROP_POWERUPS_DONTUSE	0x0400	// not used here because it doesn't seem to be set on some npc.tbl entries which DO in fact spawn powerups; see the nxflag which replaces it
#define FLAG_APPEAR_ON_FLAGID		0x0800
#define FLAG_FACES_RIGHT			0x1000
#define FLAG_SCRIPTONACTIVATE		0x2000
#define FLAG_DISAPPEAR_ON_FLAGID	0x4000
#define FLAG_SHOW_FLOATTEXT			0x8000
// NXEngine flag definitions
#define NXFLAG_FOLLOW_SLOPE			0x0001	// enable moving up/down slopes when moving horizontally
#define NXFLAG_SLOW_X_WHEN_HURT		0x0002	// move at half X speed when shaking from damage
#define NXFLAG_SLOW_Y_WHEN_HURT		0x0004	// move at half Y speed when shaking from damage
#define NXFLAG_THUD_ON_RIDING		0x0008	// if set there is a "thud" sound when player lands on it
#define NXFLAG_NO_RESET_YINERTIA	0x0010	// don't zero yinertia on blocku/blockd
#define NXFLAG_CONSOLE_ANIMATE		0x0020	// spawned at console and is implicit target of subsequent animate commands

#define NXFLAG_SLOW_WHEN_HURT		(NXFLAG_SLOW_X_WHEN_HURT | NXFLAG_SLOW_Y_WHEN_HURT)
