#include<stdint.h>

int seed;

uint32_t getrand() {
  seed = (seed * 0x343FD) + 0x269EC3;
  return seed;
}

int random(int min, int max) {
  int range, val;
  
  if(max < min) {
    min ^= max;
    max ^= min;
    min ^= max;
  }
  
  range = (max - min);
  
  val = getrand() % (range + 1);
  return val + min;
}
