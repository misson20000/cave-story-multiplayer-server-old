#include"Sprites.h"
#include"../siflib/sif.h"
#include"../siflib/sifloader.h"
#include"../siflib/sectSprites.h"

SIFSprite sprites[MAX_SPRITES];
int spritemap[OBJ_LAST];
int num_sprites;

bool load_sif(const char *filename) {
  SIFLoader sif;
  uint8_t *spritesdata;
  int spritesdatalength;
  
  if(sif.LoadHeader(filename)) {
    return 1;
  }
  
  if(!(spritesdata = sif.FindSection(SIF_SECTION_SPRITES, &spritesdatalength))) {
    return 1;
  }
  
  if(SIFSpritesSect::Decode(spritesdata, spritesdatalength, &sprites[0],
                            &num_sprites, MAX_SPRITES)) {
    return 1;
  }
  
  sif.CloseFile();
  
  for(int s=0; s<num_sprites; s++) {
    if(sprites[s].ndirs == 1) {
      sprites[s].ndirs = 2;
      
      for(int f=0; f<sprites[s].nframes; f++) {
        sprites[s].frame[f].dir[1] = sprites[s].frame[f].dir[0];
      }
    }
    
    int dx = -sprites[s].frame[0].dir[0].drawpoint.x;
    int dy = -sprites[s].frame[0].dir[0].drawpoint.y;
    
    sprites[s].bbox.offset(dx, dy);
    sprites[s].slopebox.offset(dx, dy);
    sprites[s].solidbox.offset(dx, dy);
    
    sprites[s].block_l.offset(dx, dy);
    sprites[s].block_r.offset(dx, dy);
    sprites[s].block_u.offset(dx, dy);
    sprites[s].block_d.offset(dx, dy);
    
    for(int f=0; f<sprites[s].nframes; f++) {
      for(int d=0; d<sprites[s].ndirs; d++) {
        int dx = -sprites[s].frame[f].dir[d].drawpoint.x;
        int dy = -sprites[s].frame[f].dir[d].drawpoint.y;
        sprites[s].frame[f].dir[d].pf_bbox.offset(dx, dy);
      }
    }
  }
}
