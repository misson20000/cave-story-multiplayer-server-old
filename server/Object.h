#ifndef OBJECT_H
#define OBJECT_H
#include<objtypes.h>
#define CSF 9
#include<Sprites.h>

class Map;
class PlayerMP;

#define UPDATE_MASK_POSITION 0b00000001
#define UPDATE_MASK_FRAME    0b00000010
#define UPDATE_MASK_DIR      0b00000100
#define UPDATE_MASK_SPRITE   0b00001000

typedef SIFPoint Point;

class Object {
public:
  Object(int x, int y, short id1, short id2, short type, short flags, Map *map,
         int uid);
  int x,y;   //Current
  int lx,ly; //Last delta update
  int fx,fy; //Last full update
  
  int xinertia;
  int yinertia;
  
  int frame,lframe;
  int sprite;
  
  int hp;
  int state;
  int substate;
  int timer,timer2;
  int animtimer,animframe;
  int blinktimer;
  int shaketime;
  int xmark, ymark;
  int xmark2, ymark2;
  int damage;

  bool invisible;
  bool clip_enable;

  int dirparam;
  int clipx1;
  int clipx2;
  
#define BLOCKED_MAP		1
#define BLOCKED_OBJECT	2
  union {
    struct { uint8_t blockr, blockl, blocku, blockd; };
    uint8_t block[4];
  };
  union {
    struct { uint8_t lastblockr, lastblockl, lastblocku, lastblockd; };
    uint8_t lastblock[4];
  };
  
  
  char dir,ldir;
  short id1;
  short id2;
  int uid;
  short type;
  uint32_t flags;
  uint32_t nxflags;
  int smushdamage;
  
  bool deleted;
  
  int Width();
  int Height();
  
  int CenterX();
  int CenterY();
  
  int DrawPointX();
  int DrawPointY();
  
  int Left();
  int Right();
  int Top();
  int Bottom();
  
  int SolidLeft();
  int SolidRight();
  int SolidTop();
  int SolidBottom();
  
  int ActionPointX();
  int ActionPointY();
  int ActionPoint2X();
  int ActionPoint2Y();
  
  bool apply_xinertia(int inertia);
  bool apply_yinertia(int inertia);
  
  int GetBlockingType();
  
  SIFSprite *Sprite();
  
  void SetDir(char dir);
  void SetType(int type);
  void ChangeType(short type);
  void DoAI();
  void aicmd(int id);
  void PhysicsSim();
  void SnapToGround();
  
  PlayerMP *ClosestPlayer();
  int PlayerXDistance();
  int PlayerYDistance();
  int AboveBelow(int a, int b);
  void animate_seq(int speed, const int *framelist, int nframes);
  
  int GetAttackDirection(PlayerMP *p);
  void DealContactDamage(PlayerMP *p);
  void PushPlayerOutOfWay(int xinertia, int yinertia, PlayerMP *p);
  
  Object *riding;
  Object *linkedobject;
  Map *map;
  
  void SendUpdates(uint16_t mask = UPDATE_MASK_POSITION | UPDATE_MASK_FRAME | UPDATE_MASK_DIR);
  void UpdateBlockStates(uint8_t updatemask);
  bool CheckAttribute(const Point *pointlist, int npoints, uint32_t attrmask,
                      int *tile_x = nullptr, int *tile_y = nullptr);
  bool CheckAttribute(SIFPointList *points, uint32_t attrmask,
                      int *tile_x = nullptr, int *tile_y = nullptr) {
    return CheckAttribute(&points->point[0], points->count, attrmask, tile_x,
                          tile_y);
  }
  
  bool CheckSolidIntersect(Object *other, const Point *pointlist, int npoints);
  void Delete();
  void Kill();
  void DealDamage(int dmg, Object *shot);
  
  void SpawnPowerups();
  void SpawnXP(int amt);

  //Misc. AI data
  union {
    // for player shots (not enemy shots)
    struct {
      Object *owner;
      int ttl;			// frames left till shot times out; sets range
      int dir;			// direction shot was fired in, LEFT RIGHT UP DOWN.
      int damage;			// damage dealt per hit
      
      int btype;			// bullet type
      int level;			// weapon level (0, 1, or 2)
      
      // missile boom spawner used w/ player missiles
      struct {
        int range;
        int booms_left;
      } boomspawner;
    } shot;
    
    struct {
      int bultype;
      int nlayers;
      int wave_amt;
    } mgun;
    
    struct {
      // if 1 on an OBJ_CARRIED_OBJECT then object faces in OPPOSITE direction of carrier
      bool flip;
    } carry;
    
    struct {
      int jumpheight, jumpgrav;
      int falldmg;
      bool canfly;
    } critter;
    
    struct {
      int blockedtime;
      int reachptimer;
      int tryjumptime;
      int impjumptime;
      unsigned char impjump;
      unsigned char look;
      int gunsprite;
      int changedirtimer;
      bool spawned_watershield;
    } curly;
    
    struct {
      bool left_ground;
    } toro;
    
    struct {
      Object *carried_by;
    } sue;
    
    struct {
      bool smoking;
      int smoketimer;
    } balrog;
    
    struct {
      bool fireattack;
    } igor;
    
    struct {
      bool is_horizontal;
      int x1, y1, x2, y2;
    } hvt;	// hvtrigger
    
    struct {
      Object *layers[4];
    } cloud;
  };

  void NoAnimation(); // turn off animation
  void Animate(int speed, int first, int last);
  struct {
    bool enabled;
    bool updated;
    int speed;
    int first;
    int last;
  } animation;
};

bool solidhitdetect(Object *o1, Object *o2);
bool hitdetect(Object *o1, Object *o2);

inline int Object::Width() {
  return (sprites[this->sprite].w << CSF);
}
inline int Object::Height() {
  return (sprites[this->sprite].h << CSF);
}
inline int Object::CenterX() {
  return (this->x + (Width() / 2)) - DrawPointX();
}
inline int Object::CenterY() {
  return (this->y + (Height() / 2)) - DrawPointY();
}

inline int Object::Left()			{ return (this->x + (sprites[this->sprite].bbox.x1 << CSF)); }
inline int Object::Right()			{ return (this->x + (sprites[this->sprite].bbox.x2 << CSF)); }
inline int Object::Top()			{ return (this->y + (sprites[this->sprite].bbox.y1 << CSF)); }
inline int Object::Bottom()			{ return (this->y + (sprites[this->sprite].bbox.y2 << CSF)); }

inline int Object::SolidLeft()		{ return (this->x + (sprites[this->sprite].solidbox.x1 << CSF)); }
inline int Object::SolidRight()		{ return (this->x + (sprites[this->sprite].solidbox.x2 << CSF)); }
inline int Object::SolidTop()		{ return (this->y + (sprites[this->sprite].solidbox.y1 << CSF)); }
inline int Object::SolidBottom()	{ return (this->y + (sprites[this->sprite].solidbox.y2 << CSF)); }

inline int Object::ActionPointX()	{ return (this->x + (sprites[this->sprite].frame[this->frame].dir[this->dir].actionpoint.x << CSF)); }
inline int Object::ActionPointY()	{ return (this->y + (sprites[this->sprite].frame[this->frame].dir[this->dir].actionpoint.y << CSF)); }
inline int Object::ActionPoint2X()	{ return (this->x + (sprites[this->sprite].frame[this->frame].dir[this->dir].actionpoint2.x << CSF)); }
inline int Object::ActionPoint2Y()	{ return (this->y + (sprites[this->sprite].frame[this->frame].dir[this->dir].actionpoint2.y << CSF)); }

inline int Object::DrawPointX()		{ return (sprites[this->sprite].frame[this->frame].dir[this->dir].drawpoint.x << CSF); }
inline int Object::DrawPointY()		{ return (sprites[this->sprite].frame[this->frame].dir[this->dir].drawpoint.y << CSF); }

inline SIFSprite *Object::Sprite()      { return sprites+(this->sprite); }

struct ObjProp {
  int sprite;
  int shaketime;
  
  int initial_hp;
  int xponkill;
  int damage;
  int hurt_sound, death_sound;
  int death_smoke_amt;
  
  uint32_t defaultflags;

  bool no_update_frame;
  bool no_update_pos;
  
  // AI routines
  struct {
    // executed every tick
    void (*ontick)(Object *o);
    // executed after physics sim has been done
    void (*aftermove)(Object *o);
    // if present, then when damage to the object causes it's hp to <= 0,
    // this is executed instead of destroying the object or following the
    // normal boom/spawn powerups routine.
    void (*ondeath)(Object *o);
    // executed when the object is first created or it's type is changed.
    // intended for static objects which only require a small amount of
    // initilization. This is NOT guaranteed to be only called exactly once
    // for a given object.
    void (*onspawn)(Object *o);
  } ai_routines;
};

extern ObjProp objprop[OBJ_LAST];
#endif
