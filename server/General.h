// NPC flags definitions
#define FLAG_SOLID_MUSHY			0x0001	// object blocks player but is a little "mushy" (normal solid state for enemies)
#define FLAG_IGNORETILE44			0x0002
#define FLAG_INVULNERABLE			0x0004
#define FLAG_IGNORE_SOLID			0x0008
#define FLAG_BOUNCY					0x0010	// when SOLID_BRICK also set, acts like a mini trampoline
#define FLAG_SHOOTABLE				0x0020
#define FLAG_SOLID_BRICK			0x0040	// object's entire bbox is rock-solid, just like a solid tile
#define FLAG_NOREARTOPATTACK		0x0080
#define FLAG_SCRIPTONTOUCH			0x0100
#define FLAG_SCRIPTONDEATH			0x0200
#define FLAG_DROP_POWERUPS_DONTUSE	0x0400	// not used here because it doesn't seem to be set on some npc.tbl entries which DO in fact spawn powerups; see the nxflag which replaces it
#define FLAG_APPEAR_ON_FLAGID		0x0800
#define FLAG_FACES_RIGHT			0x1000
#define FLAG_SCRIPTONACTIVATE		0x2000
#define FLAG_DISAPPEAR_ON_FLAGID	0x4000
#define FLAG_SHOW_FLOATTEXT			0x8000
#ifndef _DIRECTIONS_DEFINED
#define _DIRECTIONS_DEFINED
enum Directions {
  RIGHT	= 0,
  LEFT	= 1,
  UP		= 2,
  DOWN	= 3,
  CENTER	= 5
};
#endif
