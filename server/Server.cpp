#include<stdio.h>
#include<Server.h>
#include<Sprites.h>
#include<PlayerMP.h>
#include<string.h>

char *reason[] {
  "Reason Unknown",
  "Failed to load map"
};

Server server;
void Server::stop(int r) {
  printf("Server Stopping: %s\n", reason[r]);
  server.terminate = 1;
}

Map *Server::getMap(int no) {
  if(maps[no]) {
    return maps[no];
  } else {
    return loadMap(no, new Map(no));
  }
}

Map *Server::loadMap(int no, Map *map) {
  map->loaded = true;
  maps[no] = map;
  loaded_maps.push_front(map);
  return map;
}

void Server::unloadMap(int no) {
  maps[no]->loaded = false;
  // Would set pointer to 0 but map might reload on the same tick
}

void Server::disconnectPlayer(PlayerMP *p) {
  // Prepare the player to be deleted
  //  don't remove them from the loop yet -- it might
  //  screw things up
  
  p->disconnected = true;
  p->map->playerLeft(p);
}

void Server::saveProfile(PlayerMP *p) {
  char buf[31] = "saves/";
  strcat(buf, "username");
  strcat(buf, ".dat");
  FILE *fp = fopen(buf, "wb");

  fwrite(&p->map->id, sizeof(int), 1, fp);
  fwrite(&p->x, sizeof(int), 1, fp);
  fwrite(&p->y, sizeof(int), 1, fp);
  fwrite(&p->dir, sizeof(char), 1, fp);
  fwrite(&p->hp, sizeof(int), 1, fp);
  fwrite(&p->maxHealth, sizeof(int), 1, fp);
  
  fwrite(&p->curWeapon, sizeof(int), 1, fp);
  fwrite(p->weapons, sizeof(Weapon), WPN_COUNT, fp);

  fclose(fp);

  p->updateRespawn();

  printf("Saved to %s\n", buf);
}

void Server::loadProfile(PlayerMP *p) {
  char buf[31] = "saves/";
  strcat(buf, "username");
  strcat(buf, ".dat");
  FILE *fp = fopen(buf, "rb");

  int m; fread(&m, sizeof(int), 1, fp); p->setmap(m, 0, 0, 0);
  fread(&p->x, sizeof(int), 1, fp);
  fread(&p->y, sizeof(int), 1, fp);
  fread(&p->dir, sizeof(char), 1, fp);
  fread(&p->hp, sizeof(int), 1, fp);
  fread(&p->maxHealth, sizeof(int), 1, fp);
  
  fread(&p->curWeapon, sizeof(int), 1, fp);
  fread(p->weapons, sizeof(Weapon), WPN_COUNT, fp);

  fclose(fp);

  printf("Read from %s\n", buf);
}
