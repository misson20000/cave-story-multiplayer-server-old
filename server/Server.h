#ifndef SERVER_H
#define SERVER_H
#include<atomic>
#include<list>
#include<Map.h>
#include<Tsc.h>
#define CSF 9
#define STOP_REASON_NONE 0
#define STOP_REASON_MAPLOAD_FAIL 1
#define MP_MAX_PLAYERS 256

class Server {
private:
  std::atomic<int> terminate;
public:
  Map *maps[NUM_MAPS];
  void stop(int reason);
  void disconnectPlayer(PlayerMP *p);
  int closing() { return terminate.load(); }
  Map *getMap(int mapno); // Obtain map, loading if necessary
  Map *loadMap(int mapno, Map *map);
  void unloadMap(int mapno);
  void saveProfile(PlayerMP *p);
  void loadProfile(PlayerMP *p);
  MapData *mapdata;
  Tsc *head;
  bool flags[8000];
  std::list<PlayerMP *> players;
  std::list<Map *> loaded_maps;
};

extern Server server;
#endif
