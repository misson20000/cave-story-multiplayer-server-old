#include<thread>
#include<PlayerMP.h>
#include<NetHandler.h>
#include<Server.h>
#include<sprites.h>
#include<sounds.h>
#include<misc.h>
#include<string.h>
#include<effects.h>
#include<flags.h>

PlayerMP::PlayerMP(int sck, PlayerType type) : Object(0, 0, 0, type+1234,
      type ? OBJ_CURLYP : OBJ_QUOTEP, 0, NULL, type ? 0 : 1) {
  if(server.closing()) { //Don't even bother
    return;
  }
  
  which = type;
  stream = new NetStream(sck);
  handler = new NetHandler(stream, this);
  
  //Setup spawn
  spawn.maxHealth = 3;
  spawn.hp = spawn.maxHealth;
  spawn.mapno = STAGE_START_POINT;
  spawn.playerx = 10;
  spawn.playery = 8;
  memset(&spawn.weapons, 0, sizeof(weapons));
  memset(&skipflags, 0, sizeof(skipflags));
#ifdef START_WITH_POLAR_STAR
  printf("hehe\n");
  Weapon *pstar = spawn.weapons+WPN_POLARSTAR;
  pstar->hasWeapon = true;
  pstar->maxammo = 0;
  pstar->level = 0;
  pstar->xp = 0;
#endif
  spawn.curWeapon = WPN_POLARSTAR;
}

void PlayerMP::setmap(int map, int x, int y, int event, bool exec_asplayer) {
  this->x = (x * 16) << CSF;
  this->y = (y * 16) << CSF;
  
  if(this->map) { this->map->playerLeft(this); }
  
  this->map = server.getMap(map);
  this->map->playerEnter(this);
  handler->change_map(map, x, y);
  this->map->tsc.StartScript(&script, event);
}
Object *PlayerMP::asObject() {
  return this;
}
void PlayerMP::respawn(bool setmap, bool nofade) {
  memset(&script, 0, sizeof(ScriptInstance));
  memset(&weapons, 0, sizeof(weapons));
  firekey = 0;
  lfirekey = 0;
  dead = false;
  hide = false;
  empty_timer = 0;
  curWeapon = 0;
  disconnected = false;
  script.p = this;
  flags = flags | FLAG_SHOOTABLE;

  hurt_time = 0;

  
  if(setmap && (!this->map || (this->map && this->map->id != spawn.mapno))) {
    if(this->map) { this->map->playerLeft(this); }
    
    this->map = server.getMap(spawn.mapno);
    this->map->playerEnter(this);
    handler->change_map(spawn.mapno, spawn.playerx, spawn.playery);
    if(!nofade) {
      handler->fade_in(CENTER);
    }
  } else {
    this->x = (spawn.playerx * 16) << CSF;
    this->y = (spawn.playery * 16) << CSF;
  }
  
  this->maxHealth = spawn.maxHealth;
  this->hp = spawn.hp;
  this->curWeapon = spawn.curWeapon;
  memcpy(&weapons, &spawn.weapons, sizeof(weapons));
  getNetHandler()->update_weapons(spawn.weapons, curWeapon);
  handler->spawn(spawn.playerx, spawn.playery, spawn.maxHealth, spawn.hp);
}

// set the player state to "dead" and execute script "script"
void PlayerMP::killplayer(int script) {
  hp = 0;
  dead = true;
  hide = true;
  xinertia = 0;
  yinertia = 0;
  riding = NULL;			// why exactly did I say this? i dunno, but not touching for safety
  this->map->tsc.StartScript(&(this->script), script);
  getNetHandler()->update_hp(0, maxHealth);
}


void PlayerMP::hurt(int damage) {
  if(damage == 0) { return; }
  
  if(!this || !hp) { return; }
  
  if(hurt_time)
  { return; }
  
  if(hide)
  { return; }
  
  hp -= damage;
  getNetHandler()->Damage(damage);
  getNetHandler()->update_hp(hp, maxHealth);

  hurt_time = 128;
  
  //if (equipmask & EQUIP_WHIMSTAR)
  //  remove_whimstar(&whimstar);
  
  //if (booststate)
  //hitwhileboosting = true;
  
  if(hp <= 0) {
    getNetHandler()->sound(SND_PLAYER_DIE);
    //SmokeClouds(player, 64, 16, 16);
    
    killplayer(SCRIPT_DIED);
  } else {
    getNetHandler()->sound(SND_PLAYER_HURT);
    
    // hop
    //if (movementmode != MOVEMODE_ZEROG)
    yinertia = -0x400;
  }
  
  // decrement weapon XP.
  //if (equipmask & EQUIP_ARMS_BARRIER)
  //  SubXP(damage);
  //else
  //  SubXP(damage * 2);
}

void PlayerMP::AddWeapon(int wpn, int ammo) {
  if(!weapons[wpn].hasWeapon) {
    weapons[wpn].ammo = 0;
    weapons[wpn].maxammo = ammo;
    weapons[wpn].level = 0;
    weapons[wpn].xp = 0;
    weapons[wpn].hasWeapon = true;
    curWeapon = wpn;
    getNetHandler()->init_weapon(wpn, ammo);
  } else {
    weapons[wpn].maxammo += ammo;
  }
  
  AddAmmo(wpn, ammo);
  getNetHandler()->sound(SND_GET_ITEM);
}

void PlayerMP::AddAmmo(int wpn, int ammo) {
  weapons[wpn].ammo += ammo;
  
  if(weapons[wpn].ammo > weapons[wpn].maxammo)
  { weapons[wpn].ammo = weapons[wpn].maxammo; }
  
  getNetHandler()->add_ammo(wpn, ammo);
}

void PlayerMP::Update() {
  DoWeapons();
  if(update_queued) {
    getNetHandler()->update_player(x, y, dir, invisible);
    update_queued = false;
  }
}

void PlayerMP::GetSpriteForGun(int wpn, int look, int *spr, int *frame) {
  int s;
  
  switch(wpn) {
  case WPN_SUPER_MISSILE: s = SPR_SUPER_MLAUNCHER; break;
  
  case WPN_NEMESIS: s = SPR_NEMESIS; break;
  
  case WPN_BUBBLER: s = SPR_BUBBLER; break;
  
  case WPN_SPUR: s = SPR_SPUR; break;
  
  default:
    s = SPR_WEAPONS_START + (wpn * 2);
    break;
  }
  
  if(look) {
    s++;
    *frame = (look == DOWN);
  } else {
    *frame = 0;
  }
  
  *spr = s;
}

void PlayerMP::GetShootPoint(int *x_out, int *y_out) {
  int spr,frame;
  int x,y;
  
  GetSpriteForGun(curWeapon, look, &spr, &frame);
  
  x = this->x + (sprites[sprite].frame[this->frame].dir[dir].actionpoint.x << CSF);
  x -= sprites[spr].frame[frame].dir[this->dir].drawpoint.x << CSF;
  x += sprites[spr].frame[frame].dir[this->dir].actionpoint.x << CSF;
  
  y = this->y + (sprites[sprite].frame[this->frame].dir[dir].actionpoint.y << CSF);
  y -= sprites[spr].frame[frame].dir[this->dir].drawpoint.y << CSF;
  y += sprites[spr].frame[frame].dir[this->dir].actionpoint.y << CSF;
  
  *x_out = x;
  *y_out = y;
}

void PlayerMP::updateRespawn() {
  spawn.maxHealth = maxHealth;
  spawn.hp = hp;
  spawn.mapno = map->id;
  spawn.playerx = x;
  spawn.playery = y;
  memcpy(spawn.weapons, weapons, sizeof(weapons));
}

void PlayerMP::sendSound(int snd) {
  getNetHandler()->sound(snd);
}

void PlayerMP::AddXP(int xp) {
  Weapon *weapon = weapons + curWeapon;
  bool leveled_up = false;
  weapon->xp += xp;

  while(weapon->xp > weapon->max_xp[weapon->level]) {
    if(weapon->level < 2) {
      weapon->xp -= weapon->max_xp[weapon->level];
      weapon->level++;
      leveled_up = true;
    } else {
      weapon->xp = weapon->max_xp[weapon->level];
      break;
    }
  }

  if(curWeapon == WPN_SPUR) {
    leveled_up = false;
  }
  if(leveled_up) {
    sendSound(SND_LEVEL_UP);
    ITERATE_PLAYERS_MAP(map) {
      (*i)->getNetHandler()->effect(CenterX(), CenterY(), EFFECT_LEVELUP);
    }
  } else {
    sendSound(SND_GET_XP);
  }
  getNetHandler()->get_xp(curWeapon, xp, weapon->level);
}

void PlayerMP::AddHealth(int amt) {
  hp+= amt;
  if(hp > maxHealth) { hp = maxHealth; }
  getNetHandler()->update_hp(hp, maxHealth);
}

void PlayerMP::QueueTeleport() {
  update_queued = true;
  printf("this might've derped it up\n");
}
