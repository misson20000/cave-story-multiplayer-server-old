#include<Tsc.h>
#include<Server.h>
#include<PlayerMP.h>
#include<NetHandler.h>
#include<stdio.h>
#include<stdlib.h>
#include<stdint.h>
#include<string.h>
#include<misc.h>
#include<sprites.h>
#include<logging.h>

void TscEvent::appendOp(int op, int args[]) {
  TscOp ops;
  ops.type = op;
  ops.param.args[0] = args[0];
  ops.param.args[1] = args[1];
  ops.param.args[2] = args[2];
  ops.param.args[3] = args[3];
  vec.push_back(ops);
}

void TscEvent::appendTxt(int txtlen, char *txt) {
  TscOp ops;
  ops.type = OP_TXT;
  ops.param.txt = (char *) calloc(sizeof(char), txtlen+1);
  ops.param.txt[0] = txtlen;
  strcpy(ops.param.txt+1, txt);
  vec.push_back(ops);
}

TscOp TscEvent::getOp(int n) {
  return vec[n];
}

Tsc::Tsc() {
  memset(events, 0, sizeof(TscEvent *) * 10000);
}

void Tsc::Clear() {
  for(int i = 0; i < 10000; i++) {
    if(events[i] != 0) {
      delete events[i];
      events[i] = NULL; //Nullify pointer
    }
  }
}

// Used to decrypt TSC, but now it just loads it
char *load(char const *fname, int *fsize_out) {
  FILE *fp;
  int fsize,i;
  
  fp = fopen(fname, "rb");
  
  if(!fp) {
    fprintf(stderr, "tsc_load: No such file\n");
    return NULL;
  }
  
  fseek(fp, 0, SEEK_END);
  fsize = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  
  char *buf = (char *)malloc(fsize+1);
  fread(buf, fsize, 1, fp);
  buf[fsize] = 0;
    if(fsize_out) { *fsize_out = fsize; }
  
  return buf;
}

short readNumber(char **buf, char *buf_end) {
  static char num[5] = {0};
  int i;
  
  for(i = 0; i < 4; i++) {
    num[i] = *(*buf)++;
  }
  
  return atoi(num);
}

struct TSCCommandTable {
  const char *mnemonic;
  int nparams;
};

#include "tsc_cmdtbl.cpp"

unsigned char codealphabet[] = { "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123+-" };
unsigned char letter_to_code[256];
unsigned char mnemonic_lookup[32*32*32];

static int MnemonicToIndex(const char *str) {
  int l1, l2, l3;
  
  l1 = letter_to_code[(uint8_t)str[0]];
  l2 = letter_to_code[(uint8_t)str[1]];
  l3 = letter_to_code[(uint8_t)str[2]];
  
  if(l1==0xff || l2==0xff || l3==0xff) { return -1; }
  
  return (l1 << 10) | (l2 << 5) | l3;
}

void Tsc::GenLTC() {
  int i;
  unsigned char ch;
  
  memset(letter_to_code, 0xff, sizeof(letter_to_code));
  
  for(i=0;; i++) {
    if(!(ch = codealphabet[i])) { break; }
    
    letter_to_code[ch] = i;
  }
  
  memset(mnemonic_lookup, 0xff, sizeof(mnemonic_lookup));
  
  for(i=0; i<OP_COUNT; i++) {
    mnemonic_lookup[MnemonicToIndex(cmd_table[i].mnemonic)] = i;
  }
}

static int MnemonicToOpcode(char *str) {
  int index = MnemonicToIndex(str);
  
  if(index != -1) {
    index = mnemonic_lookup[index];
    
    if(index != 0xff) { return index; }
  }
  
  fprintf(stderr, "MnemonicToOpcode: No such command '%s'\n", str);
  return -1;
}

bool Tsc::Load(char const *fname) {
  Clear();
  int fsize;
  char *buf = load(fname, &fsize);
  
  if(!buf) {
    fprintf(stderr, "tsc_load: Could not load file %s\n", fname);
    return 1;
  }
  
  char *origbuf = buf;
  char *buf_end = (buf + (fsize - 1));
  char cmdbuf[4] = { 0 };
  int empty[] = {0};
  TscEvent *cur = NULL;
  
  while(buf + 1 <= buf_end) {
    char ch = *(buf++);
    
    if(ch == '#') {
      if(cur) {
        cur->appendOp(OP_END, empty);
        
        cur = NULL;
      }
      
      int no = readNumber(&buf, buf_end);
      
      while(buf + 1 <= buf_end) {
        if(*buf != '\r' && *buf != '\n') { break; }
        
        buf++;
      }
      
      cur = new TscEvent();
      events[no] = cur;
    } else if(ch == '<') {
      cmdbuf[0] = *(buf++);
      cmdbuf[1] = *(buf++);
      cmdbuf[2] = *(buf++);
      
      int cmd = MnemonicToOpcode(cmdbuf);
      
      if(cmd == -1) { return 1; }
      
      int nparams = cmd_table[cmd].nparams;
      int params[4] = {0};
      
      for(int i = 0; i < nparams; i++) {
        params[i] = readNumber(&buf, buf_end);
        
        if(i+1 < nparams) {
          buf++;
        }
      }
      
      cur->appendOp(cmd, params);
    } else if(cur) {
      std::vector<char> txt;
      
      while(ch != '<' && ch != '#' && buf <= buf_end) {
        if(ch != 10) {
          txt.push_back(ch);
        }
        
        ch = *(buf++);
      }
      
      buf--;
      txt.push_back(0);
      cur->appendTxt(txt.size(), txt.data());
    }
  }
  
  if(cur) {
    cur->appendOp(OP_END, empty);
  }
  
  free(origbuf);
  return 0;
}

TscEvent *Tsc::FindScript(int id) {
  if(events[id]) {
    return events[id];
  } else if(this != server.head) {
    return server.head->FindScript(id); // Maybe it's in the header TSC file
  } else {
    return NULL;
  }
}

void Tsc::StartScript(ScriptInstance *script, int id) {
  printf("Running script %d\n", id);
  TscEvent *evt = FindScript(id);
  
  if(!evt) {
    fprintf(stderr, "StartScript: No such script %d\n", id);
    return;
  }
  
  script->evt = evt;
  script->no = id;
  script->ynj_jump = -1;
  script->running = true;
  script->tsc = this;
  script->delay = 0;
  script->pc = 0;
  script->waitforkey = 0;
  script->nod_delay = 0;
  script->wait_standing = 0;
  script->target_script = 0;
  
  script->tick();
}

void Tsc::StartSynchronizedScript(SynchronizedScriptInstance *script, int id) {
  printf("Running script %d\n", id);
  TscEvent *evt = FindScript(id);
  
  if(!evt) {
    fprintf(stderr, "StartScript: No such script %d\n", id);
    return;
  }
  
  script->evt = evt;
  script->no = id;
  script->ynj_jump = -1;
  script->running = true;
  script->tsc = this;
  script->delay = 0;
  script->pc = 0;
  script->waitforkey = 0;
  script->nod_delay = 0;
  script->wait_standing = 0;
  script->target_script = 0;
}


/*
                    +--------------------+
--------------------| TSC EXECUTION CODE |-----------------
                    +--------------------+
 */

#define JUMP_IF(cond) { \
    if(cond) { \
      JumpScript(cmd.param.args[1]); \
      return; \
    } \
}

#define MAP_HANDLERS(codeashand)\
  /*    for(int j = 0; j < p->map->playercount; j++) {		\
      //p->map->players[j]->getNetHandler()->codeashand;
      }*/


char NXDir(char d) {
  switch(d) {
  case 0: return LEFT;
  
  case 1: return UP;
  
  case 2: return RIGHT;
  
  case 3: return DOWN;
  }
}

void ScriptInstance::tick() {
  if(!running) { return; }
  
  if(ynj_jump != -1) {
    if(p->getNetHandler()->ynj_ready()) {
      if(p->getNetHandler()->ynj_result() == false) {
        JumpScript(ynj_jump);
        p->getNetHandler()->ynj_hide();
        ynj_jump = -1;
      }
    } else {
      return;
    }
  }
  
  if(p->getNetHandler()->textbox_busy()) { return; }
  
  if(waitforkey) {
    if(nod_delay) {
      nod_delay--;
    } else {
      if(p->getNetHandler()->nod_release()) {
        p->getNetHandler()->nod_released();
        waitforkey = false;
      }
    }
    
    if(waitforkey) { return; }
  }
  
  if(delay) {
    delay--;
    return;
  }
  
  for(;;) {
    int cmdpc = pc++;
    TscOp cmd = evt->getOp(cmdpc);
    char *mnemonic = (char *) cmd_table[cmd.type].mnemonic;
      
    switch(cmd.type) {
    case OP_END: Terminate(); return;
    
    case OP_KEY: //KEY and PRI share the same code here, because freezing the game is bad in multiplayer
    case OP_PRI: p->getNetHandler()->lock_keys(); break;
    
    case OP_FLJ: JUMP_IF(server.flags[cmd.param.args[0]]); break;
    
    case OP_LIPLUS: AddHealth(cmd.param.args[0]); break;
    
    case OP_SOU: p->getNetHandler()->sound(cmd.param.args[0]); break;
    
    case OP_SNA: MAP_HANDLERS(sound(cmd.param.args[0])) break;
    
    case OP_AEPLUS: RefillAllAmmo(); break;
    
    case OP_MSG: p->getNetHandler()->textbox_show(); break;
    
    case OP_TXT:
      if(p->getNetHandler()->textbox_visible()) {
        p->getNetHandler()->textbox_text(cmd.param.txt);
        p->getNetHandler()->v_textbox_busy = true;
      }
      
      return;
      
    case OP_NOD:
      if(p->getNetHandler()->textbox_visible()) {
        p->getNetHandler()->nod_released();
        p->getNetHandler()->nod();
        waitforkey = true;
        return;
      }
      
      break;
      
    case OP_YNJ:
      p->getNetHandler()->textbox_ynj();
      ynj_jump = cmd.param.args[0];
      return;
      
    case OP_CLR: p->getNetHandler()->textbox_clear(); break;
    
    case OP_TRA:
      p->setmap(cmd.param.args[0], cmd.param.args[2], cmd.param.args[3],
                cmd.param.args[1]);
      return;
      
    case OP_FAI:
      p->getNetHandler()->fade_in(cmd.param.args[0]); break;
      
    case OP_FAO:
      p->getNetHandler()->fade_out(cmd.param.args[0]); break;
      
    case OP_FLPLUS:
      server.flags[cmd.param.args[0]] = 1; break;
      
    case OP_CNP:
      FOREACH_OBJECT(p->map) {
        if((*i)->id2 == cmd.param.args[0]) {
          (*i)->SetDir(NXDir(cmd.param.args[2]));
          (*i)->ChangeType(cmd.param.args[1]);
        }
      }
      break;

    case OP_ANP:
      FOREACH_OBJECT(p->map) {
        if((*i)->id2 == cmd.param.args[0]) {
          (*i)->SetDir(NXDir(cmd.param.args[2]));
          (*i)->state = cmd.param.args[1];
        }
      }
      break;

    case OP_DNP:
      FOREACH_OBJECT(p->map) {
        if((*i)->id2 == cmd.param.args[0]) {
          (*i)->Delete();
        }
      }
      break;
      
    case OP_WAI:
      delay = cmd.param.args[0]; return;
      
    case OP_TUR:
      p->getNetHandler()->textbox_tur(); break;
      
    case OP_AMPLUS:
      p->AddWeapon(cmd.param.args[0], cmd.param.args[1]); break;

    case OP_EVE:
      JumpScript(cmd.param.args[0]); break;

    case OP_SKJ: JUMP_IF(p->skipflags[cmd.param.args[0]]); break;

    case OP_MNA: p->getNetHandler()->show_map_name(); break;

    case OP_GIT: p->getNetHandler()->show_item(cmd.param.args[0]); break;

    case OP_CMU: p->getNetHandler()->music(cmd.param.args[0]); break;

    case OP_RMU: p->getNetHandler()->music(65535); break;

    case OP_CLO: p->getNetHandler()->textbox_hide(); break;

    case OP_FON: p->getNetHandler()->focus_on(cmd.param.args[0], cmd.param.args[1]); break;

    case OP_FOM: p->getNetHandler()->focus_on(0, cmd.param.args[0]); break;

    case OP_FAC: p->getNetHandler()->face(cmd.param.args[0]); break;

    case OP_SYC:
      if(p->map->tsc_sync_no == cmd.param.args[0]) {
	p->map->tsc.StartSynchronizedScript(&p->map->tsc_sync, cmd.param.args[0]);
	p->map->tsc_sync_no = 0;
      } else if(!p->map->tsc_sync_no) {
	p->map->tsc_sync_no = cmd.param.args[0];
      }
      break;

    case OP_DSY:
      fprintf(stderr, "TSC: Attempted to desynchronize in a player script!\n");
      break;

    case OP_SVP:
      server.saveProfile(p);
      break;

    case OP_INI:
      Terminate();
      p->respawn();
      p->map->tsc.StartScript(&p->script, 200);
      return;

    case OP_MYD:
      p->dir = NXDir(cmd.param.args[0]);
      p->QueueTeleport();
      break;

    case OP_HMC:
      p->invisible = true;
      p->QueueTeleport();
      break;

    case OP_SMC:
      p->invisible = false;
      p->QueueTeleport();
      break;

    case OP_MOV:
      p->x = (cmd.param.args[0] * TILE_W) << CSF;
      p->y = (cmd.param.args[1] * TILE_H) << CSF;
      p->QueueTeleport();
      break;
      
    case OP_QUA:
      nod_delay = cmd.param.args[0];
      ITERATE_PLAYERS_MAP(p->map) {
	(*i)->getNetHandler()->quake(cmd.param.args[0]);
      }
      break;
      
    default:
      nyi(mnemonic);
      break;
    }
  }
}

void ScriptInstance::Terminate() {
  if(!running) {
    return;
  }
  
  running = false;
  p->getNetHandler()->textbox_hide();
  p->getNetHandler()->unlock_keys();
  p->getNetHandler()->reset_textbox();
}

void ScriptInstance::JumpScript(int no) {
  evt = tsc->FindScript(no);
  
  if(!evt) {
    fprintf(stderr, "JumpScript: Missing script #%04d! Terminating\n", no);
    Terminate();
    return;
  }
  
  this->no = no;
  pc = 0;
  delay = 0;
  waitforkey = false;
  wait_standing = false;
  
  if(p->getNetHandler()->textbox_visible()) {
    p->getNetHandler()->textbox_hide();
    
    /*p->getNetHandler()->textbox_flag(TB_LINE_AT_ONCE, false);
    p->getNetHandler()->textbox_flag(TB_VARIABLE_WIDTH_CHARS, false);
    p->getNetHandler()->textbox_flag(TB_CURSOR_NEVER_SHOWN, false);*/
  }
}

void ScriptInstance::AddHealth(int hp) {
  //Client will be notified next time NetHandler
  //processes packets
  p->hp += hp;
  
  if(p->hp > p->maxHealth) { p->hp = p->maxHealth; }
}

void ScriptInstance::RefillAllAmmo() {

}

#define PLAYERS(cmd) ITERATE_PLAYERS_MAP(map) { \
    (*i)->cmd;					\
  }

void SynchronizedScriptInstance::tick() {
  if(!running) { return; }
  
  ITERATE_PLAYERS_MAP(map) {
    if((*i)->getNetHandler()->textbox_busy()) { return; }
  }
    
  if(delay) {
    delay--;
    return;
  }
  
  for(;;) {
    int cmdpc = pc++;
    TscOp cmd = evt->getOp(cmdpc);
    char *mnemonic = (char *) cmd_table[cmd.type].mnemonic;
      
    switch(cmd.type) {
    case OP_END: Terminate(); return;
    
    case OP_KEY: //KEY and PRI share the same code here, because freezing the game is bad in multiplayer
    case OP_PRI: PLAYERS(getNetHandler()->lock_keys()) break;
    
    case OP_FLJ: JUMP_IF(server.flags[cmd.param.args[0]]); break;
    
    case OP_LIPLUS: AddHealth(cmd.param.args[0]); break;
    
    case OP_SNA:
    case OP_SOU: PLAYERS(getNetHandler()->sound(cmd.param.args[0])); break;
    
      //se OP_AEPLUS: RefillAllAmmo(); break;
    
    case OP_MSG: PLAYERS(getNetHandler()->textbox_show()); break;
    
    case OP_TXT:
      ITERATE_PLAYERS_MAP(map) {
	PlayerMP *p = *i;
	if(p->getNetHandler()->textbox_visible()) {
	  p->getNetHandler()->textbox_text(cmd.param.txt);
	  p->getNetHandler()->v_textbox_busy = true;
	}
      }
      return;
      
      /*case OP_NOD: 
      if(p->getNetHandler()->textbox_visible()) {
        p->getNetHandler()->nod_released();
        p->getNetHandler()->nod();
        waitforkey = true;
        return;
      }
      
      break;*/
      
      /*case OP_YNJ:
      p->getNetHandler()->textbox_ynj();
      ynj_jump = cmd.param.args[0];
      return;*/
      
    case OP_CLR: PLAYERS(getNetHandler()->textbox_clear()); break;
    
    case OP_TRA:
      PLAYERS(setmap(cmd.param.args[0], cmd.param.args[2], cmd.param.args[3],
		     cmd.param.args[1]));
      return;
      
    case OP_FAI:
      PLAYERS(getNetHandler()->fade_in(cmd.param.args[0])); break;
      
    case OP_FAO:
      PLAYERS(getNetHandler()->fade_out(cmd.param.args[0])); break;
      
    case OP_FLPLUS:
      server.flags[cmd.param.args[0]] = 1; break;
      
    case OP_CNP:
      FOREACH_OBJECT(map) {
        if((*i)->id2 == cmd.param.args[0]) {
          (*i)->SetDir(NXDir(cmd.param.args[2]));
          (*i)->ChangeType(cmd.param.args[1]);
        }
      }
      break;

    case OP_ANP:
      FOREACH_OBJECT(map) {
        if((*i)->id2 == cmd.param.args[0]) {
          (*i)->SetDir(NXDir(cmd.param.args[2]));
          (*i)->state = cmd.param.args[1];
        }
      }
      break;

    case OP_DNP:
      FOREACH_OBJECT(map) {
        if((*i)->id2 == cmd.param.args[0]) {
          (*i)->Delete();
        }
      }
      break;
      
    case OP_WAI:
      delay = cmd.param.args[0]; return;
      
    case OP_TUR:
      PLAYERS(getNetHandler()->textbox_tur()); break;
      
    case OP_AMPLUS:
      PLAYERS(AddWeapon(cmd.param.args[0], cmd.param.args[1])); break;

    case OP_EVE:
      JumpScript(cmd.param.args[0]); break;

      //case OP_SKJ: JUMP_IF(p->skipflags[cmd.param.args[0]]); break;

    case OP_MNA: PLAYERS(getNetHandler()->show_map_name()); break;

    case OP_GIT: PLAYERS(getNetHandler()->show_item(cmd.param.args[0])); break;

    case OP_CMU: PLAYERS(getNetHandler()->music(cmd.param.args[0])); break;

    case OP_RMU: PLAYERS(getNetHandler()->music(65535)); break;

    case OP_CLO: PLAYERS(getNetHandler()->textbox_hide()); break;

    case OP_FON: PLAYERS(getNetHandler()->focus_on(cmd.param.args[0], cmd.param.args[1])); break;

    case OP_FOM: PLAYERS(getNetHandler()->focus_on(0, cmd.param.args[0])); break;

    case OP_FAC: PLAYERS(getNetHandler()->face(cmd.param.args[0])); break;

    case OP_SYC:
      fprintf(stderr, "TSC: Attempted to synchronize in an already synchronized script!\n");
      break;
      
    default:
      nyi(mnemonic);
      break;
    }
  }
}

void SynchronizedScriptInstance::Terminate() {
  if(!running) {
    return;
  }
  
  running = false;
  ITERATE_PLAYERS_MAP(map) {
    PlayerMP *p = *i;
    p->getNetHandler()->textbox_hide();
    p->getNetHandler()->unlock_keys();
    p->getNetHandler()->reset_textbox();
  }
}

void SynchronizedScriptInstance::JumpScript(int no) {
  evt = tsc->FindScript(no);
  
  if(!evt) {
    fprintf(stderr, "JumpScript: Missing script #%04d! Terminating\n", no);
    Terminate();
    return;
  }
  
  this->no = no;
  pc = 0;
  delay = 0;
  waitforkey = false;
  wait_standing = false;
  ITERATE_PLAYERS_MAP(map) {
    PlayerMP *p = *i;
    if(p->getNetHandler()->textbox_visible()) {
      p->getNetHandler()->textbox_hide();
    
      /*p->getNetHandler()->textbox_flag(TB_LINE_AT_ONCE, false);
	p->getNetHandler()->textbox_flag(TB_VARIABLE_WIDTH_CHARS, false);
	p->getNetHandler()->textbox_flag(TB_CURSOR_NEVER_SHOWN, false);*/
    }
  }
}

void SynchronizedScriptInstance::AddHealth(int hp) {
  ITERATE_PLAYERS_MAP(map) {
    //Client will be notified next time NetHandler
    //processes packets
    (*i)->hp += hp;
    
    if((*i)->hp > (*i)->maxHealth) { (*i)->hp = (*i)->maxHealth; }
  }
}

Tsc::~Tsc() {
  Clear();
}
