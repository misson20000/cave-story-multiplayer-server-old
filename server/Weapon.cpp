#include<Weapon.h>
#include<PlayerMP.h>
#include<sounds.h>
#include<effects.h>
#include<misc.h>
#include<sprites.h>
#include<logging.h>

struct BulletInfo {
  int sprite;
  int level;
  int frame;
  uint8_t makes_star;
  int timetolive;
  int damage;
  int speed;
  uint8_t manualsetup;
  uint8_t sound;
};

BulletInfo bullet_table[] = {
  SPR_SHOT_POLARSTAR, 	0,  0, 1, 8,   1, 0x1000, 0, SND_POLAR_STAR_L1_2,		// polarstar l1
  SPR_SHOT_POLARSTAR, 	1,  1, 1, 12,  2, 0x1000, 0, SND_POLAR_STAR_L1_2,		// polarstar l2
  SPR_SHOT_POLARSTAR_L3, 	2,  0, 1, 16,  4, 0x1000, 0, SND_POLAR_STAR_L3,		// polarstar l3
  
  SPR_SHOT_MGUN_L1, 		0,  0, 1, 20,  2, 0x1000, 0, SND_POLAR_STAR_L1_2,		// mgun l1
  
  SPR_SHOT_MGUN_L2,		1,  0, 1, 20,  4, 0x1000, 0, SND_POLAR_STAR_L1_2,		// mgun l2, white piece
  SPR_SHOT_MGUN_L2,		1,  1, 0, 21,  0, 0x1000, 0, 0,						// mgun l2, blue piece
  SPR_SHOT_MGUN_L2,		1,  2, 0, 22,  0, 0x1000, 0, 0,						// mgun l2, dark piece
  
  SPR_SHOT_MGUN_L3LEAD,	2,  0, 1, 20,  6, 0x1000, 0, SND_POLAR_STAR_L3,		// mgun l3
  SPR_SHOT_MGUN_L3TAIL,	2,  0, 0, 21,  0, 0x1000, 0, 0,						// the very long...
  SPR_SHOT_MGUN_L3TAIL,	2,  1, 0, 22,  0, 0x1000, 0, 0,						// ...4 piece trail...
  SPR_SHOT_MGUN_L3TAIL,	2,  2, 0, 23,  0, 0x1000, 0, 0,						// ...of the level 3...
  SPR_SHOT_MGUN_L3TAIL,	2,  3, 0, 24,  0, 0x1000, 0, 0,						// ...machine gun
  
  // damage for missiles is set inside missile.cpp
  SPR_SHOT_MISSILE1,		0,  0, 1, 50,  0, 0x0000, 0, SND_POLAR_STAR_L1_2,	// missile level 1
  SPR_SHOT_MISSILE2,		1,  0, 1, 65,  0, 0x0000, 0, SND_POLAR_STAR_L1_2,	// missile level 2
  SPR_SHOT_MISSILE3,		2,  0, 1, 90,  0, 0x0000, 0, SND_POLAR_STAR_L1_2,	// missile level 3
  
  SPR_SHOT_SUPERMISSILE13,0,  0, 1, 30,  0, 0x0000, 0, SND_POLAR_STAR_L1_2,	// supermissile l1
  SPR_SHOT_SUPERMISSILE2,	1,  0, 1, 40,  0, 0x0000, 0, SND_POLAR_STAR_L1_2,	// supermissile l2
  SPR_SHOT_SUPERMISSILE13,2,  0, 1, 40,  0, 0x0000, 0, SND_POLAR_STAR_L1_2,	// supermissile l3
  
  // damages are doubled because fireball can hit twice before dissipating
  SPR_SHOT_FIREBALL1,		0,  0, 1, 100, 2, 0x0000, 1, SND_FIREBALL,		// fireball l1
  SPR_SHOT_FIREBALL23,	1,  0, 1, 100, 3, 0x0000, 1, SND_FIREBALL,		// fireball l2
  SPR_SHOT_FIREBALL23,	2,  0, 1, 100, 3, 0x0000, 1, SND_FIREBALL,		// fireball l3
  
  SPR_SHOT_BLADE_L1,		0,  0, 0, 29, 15, 0x800,  0, SND_FIREBALL,		// Blade L1
  SPR_SHOT_BLADE_L2,		1,  0, 0, 17, 6,  0x800,  0, SND_FIREBALL,		// Blade L2
  SPR_SHOT_BLADE_L3,		2,  0, 0, 30, 1,  0x800,  0, SND_FIREBALL,		// Blade L3
  
  SPR_SHOT_SNAKE_L1,		0,  0, 1, 20, 4,  0x600,  2, SND_SNAKE_FIRE,	// Snake L1
  SPR_SHOT_FIREBALL23,	1,	0, 1, 23, 6,  0x200,  2, SND_SNAKE_FIRE,	// Snake L2
  SPR_SHOT_FIREBALL23,	2,	0, 1, 30, 8,  0x200,  2, SND_SNAKE_FIRE,	// Snake L3
  
  SPR_SHOT_NEMESIS_L1,	0,  0, 2, 20, 12, 0x1000, 0, SND_NEMESIS_FIRE,
  SPR_SHOT_NEMESIS_L2,	1,  0, 2, 20, 6,  0x1000, 0, SND_POLAR_STAR_L3,
  SPR_SHOT_NEMESIS_L3,	2,  0, 2, 20, 1,  0x555,  0, 0,		// 1/3 speed
  
  SPR_SHOT_BUBBLER_L1,	0,	0, 1, 40, 1,  0x600,  2, SND_BUBBLER_FIRE,
  SPR_SHOT_BUBBLER_L2,	1,	0, 1, 60, 2,  0x600,  2, SND_BUBBLER_FIRE,
  SPR_SHOT_BUBBLER_L3,	2,	0, 1, 100,2,  0x600,  2, SND_BUBBLER_FIRE,
  
  // Spur also messes with it's damage at runtime; see spur.cpp for details.
  SPR_SHOT_POLARSTAR,		0,	0, 1, 30, 4,  0x1000, 0, SND_SPUR_FIRE_1,
  SPR_SHOT_POLARSTAR,		1,	1, 1, 30, 8,  0x1000, 0, SND_SPUR_FIRE_2,
  SPR_SHOT_POLARSTAR_L3,	2,  0, 0, 30, 12, 0x1000, 0, SND_SPUR_FIRE_3,
  
  // Curly's Nemesis from Hell (OBJ_CURLY_CARRIED_SHOOTING)
  SPR_SHOT_NEMESIS_L1,	0,  0, 1, 20, 12, 0x1000, 0, SND_NEMESIS_FIRE,
  
  0, 0, 0, 0, 0, 0, 0
  
};

void PlayerMP::DoWeapons() {
  if(firekey) {
    FireWeapon();
  }

  if((weapons+curWeapon)->hasWeapon) {
    RunWeapon(firekey);
  }
  
  if(empty_timer)
  { empty_timer--; }
  
  lfirekey = firekey;
}

void PlayerMP::RunWeapon(bool firing) {
  Weapon *curweapon = weapons+curWeapon;
  int level = curweapon->level;
  
  if(firing && !curweapon->firerate[level] && lfirekey)
  { firing = false; }
  
  if((curweapon->rechargerate[level])
      && (curweapon->ammo < curweapon->maxammo)
      && !firing) {
    int rate = curweapon->rechargerate[level];
    
    if(++curweapon->rechargetimer >= rate) {
      curweapon->rechargetimer = 0;
      curweapon->ammo++;
    }
  }
  
  for(int i = 0; i < WPN_COUNT; i++) {
    if(weapons[i].firetimer)
    { weapons[i].firetimer--; }
    
    if((i != curWeapon)
        || (weapons[i].ammo >= weapons[i].maxammo)
        || firing) {
      weapons[i].rechargetimer = 0;
    }
  }
}

void SetupBullet(Map *map, Object *shot, int x, int y, int btype, int dir) {
  const BulletInfo *info = &bullet_table[btype];
  
  shot->sprite = info->sprite;
  shot->frame = info->frame;
  shot->shot.ttl = info->timetolive;
  shot->shot.damage = info->damage;
  shot->shot.level = info->level;
  shot->shot.btype = btype;
  shot->shot.dir = dir;
    
  if(info->sound) {
    ITERATE_PLAYERS_MAP(map) {
      (*i)->getNetHandler()->sound(info->sound);
    }
  }
  
  if(info->makes_star == 1) {
    ITERATE_PLAYERS_MAP(map) {
      (*i)->getNetHandler()->effect(x, y, EFFECT_STARPOOF);
    }
  }
  
  if(info->manualsetup != 1) {
    switch(dir) {
    case LEFT:
      shot->xinertia = -info->speed;
      shot->dir = LEFT;
      break;
      
    case RIGHT:
      shot->xinertia = info->speed;
      shot->dir = RIGHT;
      break;
      
    case UP:
      shot->yinertia = -info->speed;
      shot->dir = RIGHT;
      
      if(info->manualsetup != 2) { shot->sprite++; }
      
      break;
      
    case DOWN:
      shot->yinertia = info->speed;
      shot->dir = LEFT;
      
      if(info->manualsetup != 2) { shot->sprite++; }
      
      break;
    }
    
    if(info->makes_star == 2) {
      ITERATE_PLAYERS_MAP(map) {
        (*i)->getNetHandler()->effect(x+shot->xinertia/2, y, EFFECT_STARPOOF);
      }
    }

    //x -= shot->xinertia;
    //y -= shot->yinertia;
  }
  
  shot->x = x - (shot->Width() / 2);
  shot->y = y - (shot->Height() / 2);
  shot->SendUpdates(UPDATE_MASK_SPRITE | UPDATE_MASK_POSITION);
}

static Object *FireSimpleBullet(PlayerMP *p, int otype, int btype) {
  int x,y;
  
  p->GetShootPoint(&x, &y);
  Object *shot = p->map->CreateObject(0, 0, otype);

  if(p->look) { 
    SetupBullet(p->map, shot, x, y, btype, p->look);
  } else {
    SetupBullet(p->map, shot, x, y, btype, p->dir);
  }
  shot->shot.owner = p;
  return shot;
}

static Object *FireSimpleBulletOffset(PlayerMP *p, int otype, int btype,
                                      int xoff, int yoff) {
  switch(p->look ? p->look : p->dir) {
  case RIGHT: break;
  
  case LEFT: xoff = -xoff; break;
  
  case UP: SWAP(xoff, yoff); yoff = -yoff; break;
  
  case DOWN: SWAP(xoff, yoff); break;
  }
  
  Object *shot = FireSimpleBullet(p, otype, btype);
  shot->x += xoff;
  shot->y += yoff;
  shot->SendUpdates();
  ITERATE_PLAYERS_MAP(shot->map) {
    (*i)->getNetHandler()->update_object_pos(shot->uid, shot->x, shot->y);
  }
  shot->aicmd(shot->shot.dir);
  return shot;
}

void FirePolarStar(PlayerMP *p, int level) {
  //need to implement level 3 shot counting
  int xoff;
  if(level == 2) { xoff = -5 << CSF; } else { xoff = -4 << CSF; }
  
  FireSimpleBulletOffset(p, OBJ_POLAR_SHOT, B_PSTAR_L1+level, xoff, 0);
}

void PlayerMP::FireWeapon() {
  Weapon *curweapon = weapons+curWeapon;
  int level = curweapon->level;
  
  if(curweapon->firerate[level] != 0) {
    if(curweapon->firetimer) {
      return;
    } else {
      curweapon->firetimer = curweapon->firerate[level];
    }
  } else {
    if(lfirekey) { return; }
  }
  
  if(curweapon->maxammo > 0 && curweapon->ammo <= 0) {
    getNetHandler()->sound(SND_GUN_CLICK);
    
    if(empty_timer <= 0) {
      getNetHandler()->effect(CenterX(), CenterY(), EFFECT_EMPTY);
      empty_timer = 50;
    }
    
    return;
  }
  
  if(curweapon->ammo)
  { curweapon->ammo--; }
  
  switch(curWeapon) {
  case WPN_NONE: break;
  
  case WPN_POLARSTAR: FirePolarStar(this, level); break;
  
  default:
    log_debug("Cannot fire unimplemented weapon\n");
    getNetHandler()->sound(SND_BONK_HEAD);
    getNetHandler()->sound(SND_GUN_CLICK);
    break;
  }
}
