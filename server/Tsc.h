#ifndef TSC_H
#define TSC_H

#include<vector>

class PlayerMP;

enum TscOps {
  OP_AEPLUS, OP_AMPLUS, OP_AMMINUS,
  OP_AMJ, OP_ANP, OP_BOA,
  OP_BSL, OP_CAT, OP_CIL,
  OP_CLO, OP_CLR, OP_CMP,
  OP_CMU, OP_CNP, OP_CPS,
  OP_CRE, OP_CSS, OP_DNA,
  OP_DNP, OP_ECJ, OP_END,
  OP_EQPLUS, OP_EQMINUS, OP_ESC,
  OP_EVE, OP_FAC, OP_FAI,
  OP_FAO, OP_FLPLUS, OP_FLMINUS,
  OP_FLA, OP_FLJ, OP_FMU,
  OP_FOB, OP_FOM, OP_FON,
  OP_FRE, OP_GIT, OP_HMC,
  OP_INI, OP_INP, OP_ITPLUS,
  OP_ITMINUS, OP_ITJ, OP_KEY,
  OP_LDP, OP_LIPLUS, OP_MLPLUS,
  OP_MLP, OP_MM0, OP_MNA,
  OP_MNP, OP_MOV, OP_MPPLUS,
  OP_MPJ, OP_MS2, OP_MS3,
  OP_MSG, OP_MYB, OP_MYD,
  OP_NCJ, OP_NOD, OP_NUM,
  OP_PRI, OP_PSPLUS, OP_QUA,
  OP_RMU, OP_SAT, OP_SIL,
  OP_SKPLUS, OP_SKMINUES, OP_SKJ,
  OP_SLP, OP_SMC, OP_SMP,
  OP_SNP, OP_SOU, OP_SPS,
  OP_SSS, OP_STC, OP_SVP,
  OP_TAM, OP_TRA, OP_TUR,
  OP_UNI, OP_UNJ, OP_WAI,
  OP_WAS, OP_XX1, OP_YNJ,
  OP_ZAM, OP_SNA, OP_SYC,
  OP_DSY, OP_COUNT,
  OP_TXT=0xfa
};

struct TscOp {
  int type;
  int len;
  union {
    int args[4];
    char *txt;
  } param;
};

class TscEvent {
public:
  void appendOp(int op, int args[4]);
  void appendTxt(int len, char *txt);
  TscOp getOp(int loc);
private:
  std::vector<TscOp> vec;
};

class ScriptInstance;
class SynchronizedScriptInstance;
class Map;

class Tsc {
public:
  Tsc();
  ~Tsc();
  void Clear();
  bool Load(char const *fname);
  TscEvent *FindScript(int id);
  void StartScript(ScriptInstance *script, int id);
  void StartSynchronizedScript(SynchronizedScriptInstance *script, int id);
  static void GenLTC();
private:
  TscEvent *events[10000];
};

class SynchronizedScriptInstance {
public:
  TscEvent *evt;
  int pc;
  bool running;
  int no;
  int delay;
  bool waitforkey;
  int nod_delay;
  int ynj_jump;
  bool wait_standing;
  Tsc *tsc;
  Map *map;
  void tick();
  int target_script;
private:
  void Terminate();
  void JumpScript(int no);
  void AddHealth(int h);
  void RefillAllAmmo();
};

class ScriptInstance {
public:
  TscEvent *evt;
  int pc;
  bool running;
  int no;
  int delay;
  bool waitforkey;
  int nod_delay;
  int ynj_jump;
  bool wait_standing;
  Tsc *tsc;
  PlayerMP *p;
  void tick();
  int target_script;
private:
  void Terminate();
  void JumpScript(int no);
  void AddHealth(int h);
  void RefillAllAmmo();
};

// globally-accessible scripts in head.tsc
#define SCRIPT_NULL				0
#define SCRIPT_EMPTY			1		// displays a textbox that says "Empty."
#define SCRIPT_SAVE				16		// save-game script
#define SCRIPT_REFILL			17		// health refill script
#define SCRIPT_REST				19		// "Do you want to rest?"
#define SCRIPT_MISSILE_LAUNCHER	30		// spiel about missile launcher when you first get it
#define SCRIPT_DIED				40		// "You have died. Would you like to retry?"
#define SCRIPT_DROWNED			41		// "You have drowned. Would you like to retry?"
#define SCRIPT_NEVERSEENAGAIN	42		// "You were never seen again. Would you like to retry?"


#endif
