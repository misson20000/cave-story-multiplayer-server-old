#include<PlayerMP.h>
#include<Server.h>
#include<Map.h>
#include<chrono>
#include<stdint.h>
#include<ai/registry.h>
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/stat.h>
#include<netinet/in.h>
#include<netdb.h>
#include<arpa/inet.h>
#include<string.h>
#include<fcntl.h>
#include<logging.h>

#define GAME_FPS 50
#define GAME_WAIT std::chrono::milliseconds(1000/GAME_FPS)

typedef std::chrono::system_clock::duration time_p;

std::chrono::time_point<std::chrono::system_clock> start;

void AssignSprites(void);

bool Init() {
  Map::LoadData("gm/mapdata.json");
  load_sif("sprites.sif");
  Tsc::GenLTC();
  server.head = new Tsc();
  char buf[64];
  snprintf(buf, 63, "%s/data/Head.tsc", DATA_DIRECTORY);
  server.head->Load(buf);
  
  register_ai();
  AssignSprites();
  return 0;
}

time_p t() {
  return std::chrono::system_clock::now() - start;
}

PlayerType getType(int no) {
  return no ? CURLY : QUOTE;
}

int main(int argc, char *argv[]) {

  if(Init()) { //Initialize components
    log_error("Exiting due to initialization error\n");
    return 1;
  }
  
  int sock, discosock;
  struct addrinfo hints, *servinfo, *p;
  socklen_t sin_size;
  int yes=1;
  int rv;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  if((rv = getaddrinfo(NULL, "6400", &hints, &servinfo)) != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return 1;
  }
  
  for(p = servinfo; p != NULL; p = p->ai_next) {
    if((sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      continue;
    }

    if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
      log_error("setsockopt error\n");
      return 1;
    }
    
    if(bind(sock, p->ai_addr, p->ai_addrlen) == -1) {
      close(sock);
      log_error("bind error\n");
      continue;
    }

    break;
  }

  if(p == NULL) {
    log_error("Could not bind socket\n");
    return 1;
  }

  freeaddrinfo(servinfo);

  fcntl(sock, F_SETFL, O_NONBLOCK);

  if(listen(sock, 20) == -1) {
    log_error("listen error\n");
    return 1;
  }

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;
  if((rv = getaddrinfo(NULL, "6401", &hints, &servinfo)) != 0) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    return 1;
  }

  for(p = servinfo; p != NULL; p = p->ai_next) {
    if((discosock = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      continue;
    }
    if(bind(discosock, p->ai_addr, p->ai_addrlen) == -1) {
      close(discosock);
      continue;
    }

    break;
  }

  if(p == NULL) {
    log_error("Failed to bind discovery socket!\n");
    return 1;
  }

  freeaddrinfo(servinfo);

  printf("Started! Accepting connections...\n");
  
  start = std::chrono::system_clock::now();
  time_p nexttick = t();
  int s;
  int playercount = 0;
  
  while(!server.closing()) {
    //addr_len = sizeof sockaddr_storage;
    //#error HALF FINISHED CODE HERE


    if((s = accept(sock, NULL, NULL)) > 0) {
      // Connect new player
      PlayerMP *p;
      server.players.push_front(p = new PlayerMP(s, getType(playercount)));
      // Setup new player
      p->getNetHandler()->handshake();
      p->respawn(true, true);
#ifndef SKIP_STARTING_CUTSCENE
      p->map->tsc.StartScript(&p->script, 200);
#endif
      playercount++;
    }
    
    time_p curtime = t();
    time_p remain = nexttick - curtime;
    
    if(remain <= time_p(0)) {
      //Run normal tick
      for(std::list<PlayerMP *>::iterator i = server.players.begin();
          i != server.players.end(); i++) {
        if(!((*i)->disconnected)) { // Keep them around so they don't screw up the loop
          (*i)->getNetHandler()->handlePackets();
          
          if((*i)->script.running) { (*i)->script.tick(); }
          
          (*i)->Update();
        }
      }
      
      // Unload maps before AI because sometimes AI segfaults when there are no players in map
      for(std::list<Map *>::iterator i = server.loaded_maps.begin();
          i != server.loaded_maps.end();) {
        if(!(*i)->loaded) {
          server.maps[(*i)->id] = 0;
          delete *i;
          i = server.loaded_maps.erase(i);
        } else {
          i++;
        }
      }
      
      for(std::list<Map *>::iterator i = server.loaded_maps.begin();
          i != server.loaded_maps.end(); i++) {
        (*i)->DoAI();
      }
      
      for(std::list<PlayerMP *>::iterator i = server.players.begin();
          i != server.players.end();) {
        // Time to get rid of disconnected players
        //  with our special loop
        if((*i)->disconnected) {
          delete *i;
          i = server.players.erase(i);
        } else {
          i++;
        }
      }
      
      nexttick = curtime + GAME_WAIT;
    } else {
      remain /= 2;
      
      if(remain > time_p(1)) {
        std::this_thread::sleep_for(remain);
      }
    }
    
  }
  
  Map::DeleteData();
  return 0;
}
