#ifndef PLAYERMP_H
#define PLAYERMP_H

#include<NetStream.h>
#include<NetHandler.h>
#include<Map.h>
#include<Object.h>
#include<Tsc.h>
#include<Weapon.h>

enum PlayerType {
  QUOTE = 0,
  CURLY = 1,
};

class PlayerMP : public Object {
public:
  PlayerMP(int sck, PlayerType type);
  NetHandler *getNetHandler() { return handler; }
  /*
    map = ID of map to switch to
    x = X position to warp to
    y = Y position to warp to
    event = Event to execute upon entry
    exec_asplayer = Execute the script as player or not
   */
  void setmap(int map, int x, int y, int event, bool exec_asplayer=true);
  void respawn(bool setmap=true, bool nofade=true);
  PlayerType which;
  int maxHealth;
  int hurt_time;
  char dead;
  bool hide;
  Object *asObject();
  ScriptInstance script;
  void hurt(int damage);
  void AddXP(int xo);
  void SubXP(int xp);
  void AddWeapon(int id, int ammo);
  void AddAmmo(int id, int ammo);
  void AddHealth(int amt);
  void sendSound(int snd);
  int curWeapon;
  bool firekey;
  bool lfirekey;
  void Update();
  void GetSpriteForGun(int wpn, int look, int *spr, int *frame);
  void GetShootPoint(int *x, int *y);

  void QueueTeleport(); //more like queue update of position, visibility, and direction
  
  bool disconnected;

  bool skipflags[9999];

  Weapon weapons[WPN_COUNT];
  void updateRespawn();

  char look;
private:
  //Located in Weapon.cpp
  void DoWeapons();
  void FireWeapon();
  void RunWeapon(bool f);
  //Located in PlayerMP.cpp
  void killplayer(int script);
  Object *o;
  NetStream *stream;
  NetHandler *handler;
  struct {
    int maxHealth;
    int hp;
    int mapno;
    int playerx;
    int playery;
    Weapon weapons[WPN_COUNT];
    int curWeapon;
  } spawn;
  int empty_timer;
  bool update_queued;
};

#endif
