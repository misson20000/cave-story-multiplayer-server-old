#include<Server.h>
#include<Map.h>
#include<General.h>
#include<PlayerMP.h>
#include<config.h>
#include<logging.h>

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<jansson.h>
unsigned int Map::tilekey[256];

void Map::playerEnter(PlayerMP *p) {
  NetHandler *h = p->getNetHandler();
  h->clear_objects();
  FOREACH_OBJECT(this) {
    Object *o = *i;
    //don't forget to sync ai state and stuff later, Xenotoad!
    h->send_object(o);
    
    if(o->animation.enabled) {
      h->animate(o->uid, o->animation.speed, o->animation.first, o->animation.last);
    }
  }
  ITERATE_PLAYERS_MAP(this) {
    // Send other player objects to new player
    h->send_object((*i)->asObject());
    // Send new player object to other players
    (*i)->getNetHandler()->send_object(p->asObject());
  }
  players.push_back(p);
}

void Map::playerLeft(PlayerMP *p) {
  //player actually gets removed in removePlayers()
}

bool Map::removePlayers() {
  PlayerMP *playersToRemove[players.size()];
  int nrem = 0;
  for(std::list<PlayerMP *>::iterator i = players.begin(); i != players.end();) {
    if((*i)->map != this) {
      playersToRemove[nrem++] = *i;
      i = players.erase(i);
    } else {    
      i++;
    }
  }
  
  if(players.empty()) {
    server.unloadMap(id);
    return false;
  }

  for(int j = 0; j < nrem; j++) {
    ITERATE_PLAYERS_MAP(this) {
      (*i)->getNetHandler()->kill_object(playersToRemove[j]->asObject());
    }
  }
  return true;
}

Object *Map::CreateObject(int x, int y, int type, int xinertia, int yinertia,
                          int dir, Object *linkedobject, int flag, int evt, int uid, int flags) {
  Object *o;
  if(flags == -1) {
    flags = objprop[type].defaultflags;
  }
  if(uid == -1) {
    uid = maxuid++;
  }
  o = new Object(x, y, flag, evt, type, flags, this, uid);
  o->dir = dir;
  o->xinertia = xinertia;
  o->yinertia = yinertia;
  o->linkedobject = linkedobject;  
  objects.push_back(o);
  
  ITERATE_PLAYERS_MAP(this) {
    (*i)->getNetHandler()->send_object(o);
  }
  return o;
}

int Map::LoadData(char *mapdata) {
  json_t *root = json_load_file(mapdata, 0, NULL);
  if(!json_is_array(root)) { log_error("mapdata.json: Root is not an array!\n"); return 1; }

  server.mapdata = new MapData[json_array_size(root)];
  for(int i = 0; i < json_array_size(root); i++) {
    json_t *map = json_array_get(root, i);

    int id = json_integer_value(json_object_get(map, "id"));
    strcpy(server.mapdata[id].filename,
	   json_string_value(json_object_get(map, "filename")));
    strcpy(server.mapdata[id].tileset,
	   json_string_value(json_object_get(map, "tileset")));
    strcpy(server.mapdata[id].caption,
	   json_string_value(json_object_get(map, "caption")));
    server.mapdata[id].bossNo = json_integer_value(json_object_get(map, "bossNo"));

  }
  json_decref(root);

  FILE *tilekeyf = fopen("tilekey.dat", "rb");
  if(tilekeyf <= 0) {
    log_error("Could not open tilekey.dat\n");
  }
  fread(tilekey, sizeof(unsigned int), 256, tilekeyf);
  fclose(tilekeyf);
}
void Map::DeleteData() {
  delete[] server.mapdata;
}
Map::Map(int no) {
  printf("Loading Map %d\n", no);
  char fnamepxm[48];
  sprintf(fnamepxm, "%s/data/Stage/%s.pxm", DATA_DIRECTORY, server.mapdata[no].filename);
  char fnamepxa[48];
  sprintf(fnamepxa, "%s/data/Stage/%s.pxa", DATA_DIRECTORY, server.mapdata[no].tileset);
  char fnamepxe[48];
  sprintf(fnamepxe, "%s/data/Stage/%s.pxe", DATA_DIRECTORY, server.mapdata[no].filename);
  char fnametsc[48];
  sprintf(fnametsc, "%s/data/Stage/%s.tsc", DATA_DIRECTORY, server.mapdata[no].filename);
  
  if(load_pxm(fnamepxm) || load_pxa(fnamepxa) || load_pxe(fnamepxe)
      || tsc.Load(fnametsc)) {
  }
  
  id = no;
  tsc_sync.map = this;
}
int Map::load_pxa(char *fnamepxa) {
  FILE *pxa = fopen(fnamepxa, "rb");
  
  if(!pxa) {
    log_error("Mapload Error: Could not open PXA file\n");
    server.stop(STOP_REASON_MAPLOAD_FAIL);
    return 1;
  }
  
  fread(&tilecode, sizeof(char), 256, pxa);
  int i;
  
  for(i = 0; i < 256; i++) {
    tileattr[i] = tilekey[tilecode[i]];
  }
  
  fclose(pxa);
}
void Map::DoAI() {
  if(tsc_sync.running) {
    tsc_sync.tick();
  }
  
  if(removePlayers()) {
    FOREACH_OBJECT(this) {
      (*i)->DoAI();
      (*i)->SendUpdates();
      (*i)->PhysicsSim();
    }
  }
  
  //Remove deleted objects
  for(std::list<Object *>::iterator i = objects.begin(); i != objects.end();) {
    if((*i)->deleted) {
      delete *i;
      i = objects.erase(i);
    } else {
      i++;
    }
  }
  
}
Map::~Map() {
  while(!objects.empty()) { delete objects.front(), objects.pop_front(); }
  
  log_info("Unloading Map\n");
}
int Map::load_pxe(char *fnamepxe) {
  FILE *pxe = fopen(fnamepxe, "rb");
  
  if(!pxe) {
    log_error("Mapload Error: Could not open PXE file\n");
    server.stop(STOP_REASON_MAPLOAD_FAIL);
    return 1;
  }
  
  if(fgetc(pxe) == 'P' &&
      fgetc(pxe) == 'X' &&
      fgetc(pxe) == 'E' &&
      fgetc(pxe) == 0) {
    int n;
    int i2 = 0;
    fread(&n, sizeof(int), 1, pxe);
    
    for(int i = 0; i < n; i++) {
      Object *o;
      short buf[6];
      fread(buf, sizeof(short), 6, pxe);
      int x = buf[0];
      int y = buf[1];
      int flag = buf[2];
      int evt = buf[3];
      int typ = buf[4];
      int flags = buf[5];
      
      int dir = (flags & FLAG_FACES_RIGHT) ? RIGHT : LEFT;
      
      if(typ || flag || evt || flags) {
        if((flags & FLAG_APPEAR_ON_FLAGID    &&  server.flags[flag]) ||
            (flags & FLAG_DISAPPEAR_ON_FLAGID && !server.flags[flag]) ||
            !(flags & (FLAG_APPEAR_ON_FLAGID | FLAG_DISAPPEAR_ON_FLAGID))) {
          if(typ == OBJ_CHEST_OPEN) { y++; }
          
          o = CreateObject((x * TILE_W) << CSF, (y * TILE_H) << CSF, typ, 0, 0, 0, NULL,
			   flag, evt, i2 + MP_MAX_PLAYERS, flags);
          ID2Lookup[evt] = o;
          
          i2++;
        }
      }
    }
    
    maxuid = i2 + MP_MAX_PLAYERS;
    return 0;
  } else {
    log_error("Mapload Error: File is not a PXE file\n");
    server.stop(STOP_REASON_MAPLOAD_FAIL);
    return 1;
  }
}
int Map::load_pxm(char *fnamepxm) {
  FILE *pxm = fopen(fnamepxm, "rb");
  
  if(!pxm) {
    log_error("Mapload Error: Could not open PXM file\n");
    server.stop(STOP_REASON_MAPLOAD_FAIL);
    return 1;
  }
  
  if(fgetc(pxm) == 'P' &&
      fgetc(pxm) == 'X' &&
      fgetc(pxm) == 'M' &&
      fgetc(pxm) == 0x10) {
    char buf[4];
    fread(buf, sizeof(char), 4, pxm);
    xsize = buf[0] + (buf[1] << 16);
    ysize = buf[2] + (buf[3] << 16);
    tiles = new uint8_t *[xsize];
    
    for(int x = 0; x < xsize; x++) {
      tiles[x] = new uint8_t[ysize];
    }
    
    for(int y = 0; y < ysize; y++) {
      for(int x = 0; x < xsize; x++) {
        tiles[x][y] = fgetc(pxm);
      }
    }
  } else {
    log_error("Mapload Error: File is not a PXM file\n");
    server.stop(STOP_REASON_MAPLOAD_FAIL);
    fclose(pxm);
    return 1;
  }
  
  fclose(pxm);
  return 0;
}

void Map::SmokeClouds(Object *o, int nclouds, int rangex, int rangey, Object *push_behind) {
  ITERATE_PLAYERS_MAP(this) {
    (*i)->getNetHandler()->smoke_clouds(o->CenterX(), o->CenterY(), nclouds, rangex, rangey, push_behind);
  }
}
