#ifndef NETHANDLER_H
#define NETHANDLER_H
#include<thread>
#include<NetStream.h>
#include<Object.h>
#include<Weapon.h>
class PlayerMP;

#define PACKET_BYTE(b) stream->writeByte(b)
#define PACKET_SHORT(s) stream->writeShort(s)
#define PACKET_INT(i) stream->writeInt(i)

class NetHandler {
public:
  NetHandler(NetStream *str, PlayerMP *ply);
  ~NetHandler();
  
  void handshake();
  void change_map(int mapno, int x, int y);
  void spawn(int x, int y, int maxhp, int hp);
  void update_pos(int x, int y);
  void send_object(Object *obj);
  void clear_objects();
  void kill_object(Object *obj);
  void update_object_pos(short id2, int x, int y);
  void update_object_frame(short id2, int frame);
  void update_object_dir(short id2, uint8_t dir);
  void effect(int x, int y, short type);
  bool ynj_ready() { return v_ynj_ready; }
  bool ynj_result() { return v_ynj_result; }
  void ynj_hide() { PACKET_BYTE(12); PACKET_BYTE(1); }
  bool textbox_busy() { return v_textbox_busy; }
  bool textbox_visible() { return v_textbox_visible; }
  bool nod_release() { return v_nod_release; }
  void nod_released() { v_nod_release = false; }
  void sound(int s) { PACKET_BYTE(13); PACKET_INT(s); }
  void textbox_clear() { PACKET_BYTE(12); PACKET_BYTE(0); }
  void textbox_hide() { PACKET_BYTE(12); PACKET_BYTE(5); v_textbox_visible = false; }
  void textbox_show() { PACKET_BYTE(12); PACKET_BYTE(2); v_textbox_visible = true; }
  void textbox_ynj() { PACKET_BYTE(12); PACKET_BYTE(6); v_ynj_ready = false; }
  void textbox_tur() { PACKET_BYTE(12); PACKET_BYTE(7); }
  void textbox_text(char *txt);
  void nod() { PACKET_BYTE(12); PACKET_BYTE(4); }
  void lock_keys()   { PACKET_BYTE(14); PACKET_BYTE(1); v_locked_keys = true; }
  void unlock_keys() { PACKET_BYTE(14); PACKET_BYTE(0); v_locked_keys = false; }
  void update_hp(char hp, char maxhp) { PACKET_BYTE(15); PACKET_BYTE(hp); PACKET_BYTE(maxhp); }
  void fade_in(int dir) { PACKET_BYTE(16); PACKET_BYTE(0); PACKET_BYTE(dir); }
  void fade_out(int dir) { PACKET_BYTE(16); PACKET_BYTE(1); PACKET_BYTE(dir); }
  void update_object_type(short id2, short type) { PACKET_BYTE(17); PACKET_SHORT(id2); PACKET_SHORT(type); }
  void Damage(int dmg) { PACKET_BYTE(18); PACKET_SHORT(dmg); }
  void delta_pos_x_byte(short uid, signed char x) { PACKET_BYTE(19); PACKET_SHORT(uid); PACKET_BYTE(x); }
  void delta_pos_y_byte(short uid, signed char y) { PACKET_BYTE(20); PACKET_SHORT(uid); PACKET_BYTE(y); }
  void delta_pos_xy_byte(short uid, signed char x, signed char y) { PACKET_BYTE(21); PACKET_SHORT(uid); PACKET_BYTE(x); PACKET_BYTE(y); }
  void delta_pos_x_short(short uid, signed short x) { PACKET_BYTE(22); PACKET_SHORT(uid); PACKET_SHORT(x); }
  void delta_pos_y_short(short uid, signed short y) { PACKET_BYTE(23); PACKET_SHORT(uid); PACKET_SHORT(y); }
  void delta_pos_xy_short(short uid, signed short x, signed short y) { PACKET_BYTE(24); PACKET_SHORT(uid); PACKET_SHORT(x); PACKET_SHORT(y); }
  void update_pos_no_csf(short uid, int x, int y) { PACKET_BYTE(25); PACKET_SHORT(uid); PACKET_INT(x); PACKET_INT(y); }
  void do_delta(short uid, int x, int y, int lx, int ly);
  void do_delta_x(short uid, int x, int y, int lx);
  void do_delta_y(short uid, int x, int y, int ly);
  void animate(short uid, char speed, short first, short last) { PACKET_BYTE(26); PACKET_SHORT(uid); PACKET_BYTE(speed); PACKET_SHORT(first); PACKET_SHORT(last); }
  void init_weapon(char id, short ammo) { PACKET_BYTE(27); PACKET_BYTE(id); PACKET_SHORT(ammo); }
  void add_ammo(char id, short ammo) { PACKET_BYTE(28); PACKET_BYTE(id); PACKET_SHORT(ammo); }
  void set_sprite(short uid, short sprite) { PACKET_BYTE(29); PACKET_SHORT(uid); PACKET_SHORT(sprite); }
  void update_block(short x, short y, char t) { PACKET_BYTE(30); PACKET_SHORT(x); PACKET_SHORT(y); PACKET_BYTE(t); }
  void deal_damage(short uid, short dmg) { PACKET_BYTE(31); PACKET_SHORT(uid); PACKET_SHORT(dmg); }
  void delete_object(short uid) { PACKET_BYTE(32); PACKET_SHORT(uid); }
  void show_map_name() { PACKET_BYTE(33); }
  void show_item(short no) { PACKET_BYTE(12); PACKET_BYTE(8); PACKET_SHORT(no); }
  void music(unsigned short no) { PACKET_BYTE(34); PACKET_SHORT(no); }
  void focus_on(short id2, short time) { PACKET_BYTE(35); PACKET_SHORT(id2); PACKET_SHORT(time); }
  void face(short face) { PACKET_BYTE(12); PACKET_BYTE(9); PACKET_SHORT(face); }
  void reset_textbox() { PACKET_BYTE(12); PACKET_BYTE(10); }
  void smoke_clouds(int x, int y, int nclouds, int rangex, int rangey, Object *push_behind);
  void get_xp(int w, int xp, int lvl) { PACKET_BYTE(37); PACKET_BYTE(w); PACKET_BYTE(lvl); PACKET_BYTE(xp); }
  void update_weapons(Weapon *ptr, int curWeapon);
  void object_ai(short uid, short command) { PACKET_BYTE(39); PACKET_SHORT(uid); PACKET_SHORT(command); }
  void quake(int time) { PACKET_BYTE(40); PACKET_INT(time); }
  void update_player(int x, int y, char dir, char visibility)
  { PACKET_BYTE(41); PACKET_INT(x); PACKET_INT(y);
    PACKET_BYTE(dir); PACKET_BYTE(visibility); }
  
  void ynj_result(bool result);
  void release_nod() { v_nod_release = true; }
  void handlePackets();
  bool v_textbox_busy;
  bool v_locked_keys;
private:
  NetStream *stream;
  PlayerMP *player;
  bool v_ynj_ready;
  bool v_ynj_result;
  bool v_textbox_visible;
  bool v_nod_release;
  
  int l_hp;
  int l_maxhp;
};
#endif
